﻿import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render } from "@testing-library/react";

import Login from '../../src/pages/login.jsx'
import '@testing-library/jest-dom';

describe('login', () => {
    /**
     * @jest-environment jsdom
     */
    test('user id input', () => {
        let screen = render(<BrowserRouter><Login /></BrowserRouter>)
        const input = screen.getByRole('textbox')
        expect(input).toBeInTheDocument()
    })
})