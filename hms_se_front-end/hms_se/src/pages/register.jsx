﻿import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import { TabPanel, TabList, TabContext } from '@mui/lab';
import { FormControl, OutlinedInput, Tab, Box, InputLabel, Button, TextField, MenuItem, InputAdornment, Divider } from '@mui/material'
import { NextPlan, AccountBox, More, AddIcCall } from '@mui/icons-material';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { valid, format } from '../utility/tool.jsx'
import { GenderList, AlterList, option, RegisterURL, EmailPattern, PhoneNumberPattern, PasswordPattern, NumberPattern, InCompleteInput, UncorretPassword, NonNumeric, UncorrectPhoneNumber, UncorrectEmail, UnmatchedPassword, RegisterSuccess } from '../utility/constants.jsx'
import { Password, Submit } from '../utility/myComponents.jsx';
import $ from 'jquery';
import axios from 'axios';

export default function Register(props) {
    useEffect(() => {
        if (props.state.login) {
            nav('/home')
            return;
        }

        $('#user_id').focus()
    }, [])
    //--------------------------------------------------------------------------------
    // hooks
    //--------------------------------------------------------------------------------
    // router
    let nav = useNavigate()
    // state
    const [state, setState] = React.useState({
        data: {
            username: '',
            password: '',
            confirmPassword: '',
            name: '',
            gender: 9,
            birthday: null,
            height: '',
            weight: '',
            phoneNumber: '',
            email: '',
            beenDrinking: false,
            beenSmoking: false
        },
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isHeightValid: true,
        isWeightValid: true,
        isPhoneNumberValid: true,
        isEmailValid: true
    })
    // tab
    const [value, setValue] = React.useState('1');

    // show password
    const [showPassword, setShowPassword] = useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = useState(false)

    //--------------------------------------------------------------------------------
    // actions
    //--------------------------------------------------------------------------------
    //回车键
    const handleKey = (e) => {
        if (e.key == 'Enter') {
            switch (e.target.id) {
                case 'user_id': $("#user_password").focus(); break;
                case 'user_password': $("#confirm_password").focus(); break;
                case 'confirm_password': $('#tab1_next').click(); break;
                case 'user_name': $("#gender").focus(); break;
                case 'height': $("#weight").focus(); break;
                case 'weight': $('#tab2_next').click(); break;
                case 'phone_number': $('#email').focus(); break;
                case 'email': submitButtonClick(); break;
                default: break;
            }

        }
    }

    // tab点击
    const handleTabClick = async (e, v) => {
        setValue(v)
        switch (v) {
            case '1': setTimeout(() => $("#user_id").focus(), 10); break;
            case '2': setTimeout(() => $("#user_name").focus(), 10); break;
            case '3': setTimeout(() => $("#phone_number").focus(), 10); break;
            default: break;
        }
    }

    // 显示密码按钮
    const handlePasswordShowClick = (callback, target) => {
        callback()
        $('#' + target).focus()
    }

    // 下一步按钮
    const nextButtonClick = async (e) => {
        if (e.target.id == 'tab1_next') {
            setValue("2")
            setTimeout(() => $("#user_name").focus(), 10)
        } else if (e.target.id == 'tab2_next') {
            setValue("3")
            setTimeout(() => $("#phone_number").focus(), 10)
        }
    }

    // 提交按钮
    const submitButtonClick = async () => {
        // 检查输入是否完整
        if (state.data.username.length == 0) { props.setMessage(format(InCompleteInput, '用户名'), true, "warning"); return; }
        if (state.data.password.length == 0) { props.setMessage(format(InCompleteInput, '密码'), true, "warning"); return; }
        if (state.data.confirmPassword.length == 0) { props.setMessage(format(InCompleteInput, '密码确认'), true, "warning"); return; }

        // 检查输入格式是否正确
        if (!valid(state.data.password, PasswordPattern, 'g')) { props.setMessage(UncorretPassword, true, "warning"); return; }
        if (!valid(state.data.height, NumberPattern, 'm', true)) { props.setMessage(format(NonNumeric, '身高'), true, "warning"); return; }
        if (!valid(state.data.weight, NumberPattern, 'm', true)) { props.setMessage(format(NonNumeric, '体重'), true, "warning"); return; }
        if (!valid(state.data.phoneNumber, PhoneNumberPattern, 'm', true)) { props.setMessage(UncorrectPhoneNumber, true, "warning"); return; }
        if (!valid(state.data.email, EmailPattern, 'm', true)) { props.setMessage(UncorrectEmail, true, "warning"); return; }

        // 检查密码确认
        if (state.data.password != state.data.confirmPassword) { props.setMessage(UnmatchedPassword, true, "warning"); return; }

        const userInfo = {
            username: state.data.username,
            password: state.data.password,
            name: state.data.name,
            gender: state.data.gender,
            birthday: state.data.birthday ? state.data.birthday.format("YYYY-MM-DD") : state.data.birthday,
            height: state.data.height,
            weight: state.data.weight,
            phone_number: state.data.phoneNumber,
            email: state.data.email,
            been_drinking: state.data.beenDrinking,
            been_smoking: state.data.beenSmoking,
            is_premium: false,
        }

        axios.post(RegisterURL, userInfo, option)
            .then(res => {
                if (res.status == 200) {
                    if (res.data.status == "success") {
                        props.setMessage(RegisterSuccess, true, "success")
                        nav("/login")
                    } else {
                        props.setMessage(res.data.message, true, "warning")
                    }
                }
            })
            .catch(err => {
                props.setMessage(err.message, true, "error")
            })
    }

    //--------------------------------------------------------------------------------
    // page
    //--------------------------------------------------------------------------------
    return (
        <div>
            <Box sx={{ flexGrow: 1, display: 'flex', width: 800,height: 300 }}>
                <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleTabClick} orientation="vertical" variant="scrollable">
                            <Tab
                                id="account_tab"
                                icon={<AccountBox />}
                                iconPosition="start"
                                label="账号"
                                value="1"
                                sx={{ width: 125, height: 72 }} />
                            <Tab
                                id="user_info_tab"
                                icon={<More />}
                                iconPosition="start"
                                label="个人信息"
                                value="2"
                                sx={{ width: 125, height: 72 }}/>
                            <Tab
                                id="contact_info_tab"
                                icon={<AddIcCall />}
                                iconPosition="start"
                                label="联系方式"
                                value="3"
                                sx={{ width: 125, height: 72 }}/>
                        </TabList>
                    </Box>
                    {/* 用户ID */}
                    <TabPanel value="1" sx={{ width: 675, height: 300 }}>
                        <Box sx={{ pb: '8px' }}>
                            <TextField
                                id="user_id"
                                label="用户ID"
                                sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                defaultValue={state.data.username}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        username: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        username: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}
                                placeholder="用户名"
                            />
                        </Box>
                        {/* 密码 */}
                        <Box sx={{ pb: '8px' }}>
                            <Password
                                text="密码"
                                id="user_password"
                                showPassword={showPassword}
                                placeholder="密码"
                                defaultValue={state.data.password}
                                onClick={() => handlePasswordShowClick(() => setShowPassword((show) => (!show)), 'user_password')}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        password: tar.target.value
                                    },
                                    isPasswordValid: true,
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        password: tar.target.value
                                    },
                                    isPasswordValid: !tar.target.value.length || valid(tar.target.value, PasswordPattern, 'g'),
                                    isConfirmPasswordValid: state.data.confirmPassword == "" ? true : (!tar.target.value.length || (valid(state.data.confirmPassword, PasswordPattern, 'g') && state.data.password == state.data.confirmPassword))
                                })}
                                onKeyUp={handleKey}
                                error={!state.isPasswordValid}
                            />
                        </Box>
                        {/* 确认密码 */}
                        <Box sx={{ pb: '16px' }}>
                            <Password
                                text="密码确认"
                                id="confirm_password"
                                showPassword={showConfirmPassword}
                                placeholder="确认密码"
                                defaultValue={state.data.confirmPassword}
                                onClick={() => handlePasswordShowClick(() => setShowConfirmPassword((show) => (!show)), 'confirm_password')}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        confirmPassword: tar.target.value
                                    },
                                    isConfirmPasswordValid: true,
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        confirmPassword: tar.target.value
                                    },
                                    isConfirmPasswordValid: (!tar.target.value.length || (valid(tar.target.value, PasswordPattern, 'g') && state.data.password == state.data.confirmPassword))
                                })}
                                onKeyUp={handleKey}
                                error={!state.isConfirmPasswordValid}
                            />
                        </Box>
                        <Divider />
                        <Box sx={{ mt: '16px' }}>
                            <Button
                                id="tab1_next"
                                variant="contained"
                                endIcon={<NextPlan />}
                                onClick={nextButtonClick}>下一步</Button>
                        </Box>
                    </TabPanel>
                    <TabPanel value="2" sx={{ width: 675, height: 300 }}>
                        <Box sx={{ display: 'flex', flexWrap: 'nowrap', justifyContent: 'center' }}>
                            {/* 姓名 */}
                            <TextField
                                id="user_name"
                                label="姓名"
                                sx={{ m: 1, width: '35ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                defaultValue={state.data.name}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        name: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        name: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey} />
                            {/* 性别 */}
                            <TextField
                                id="gender"
                                label="性别"
                                select
                                sx={{ m: 1, width: '11.4ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={state.data.gender}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        gender: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        gender: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}>
                                {GenderList.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </Box>
                        {/* 生日 */}
                        <FormControl id="birthday_input" sx={{ m: 1,pb: '8px', width: '48ch', position: "relative" }} variant="outlined" onKeyUp={(e) => { if (e.key == 'Enter') $('#height').focus() }}>
                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                <DemoContainer components={['DatePicker']}>
                                    <DatePicker
                                        id="birthday"
                                        sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                                        color="primary"
                                        label="出生日期"
                                        defaultValue={state.data.birthday}
                                        onChange={(tar) => setState({
                                            ...state,
                                            data: {
                                                ...state.data,
                                                birthday: tar
                                            }
                                        })}
                                        onBlur={(tar) => setState({
                                            ...state,
                                            data: {
                                                ...state.data,
                                                birthday: tar
                                            }
                                        })}
                                        onKeyUp={handleKey}
                                        disableFuture
                                    />
                                </DemoContainer>
                            </LocalizationProvider>
                        </FormControl>
                        <Box sx={{ pb: '16px' }}>
                            {/* 身高 */}
                            <FormControl id="height_input" sx={{ m: 1, width: '23.2ch', position: "relative" }} variant="outlined">
                                <InputLabel htmlFor="height" >身高</InputLabel>
                                <OutlinedInput
                                    id="height"
                                    color="primary"
                                    sx={{ bgcolor: 'white' }}
                                    endAdornment={<InputAdornment position="end">cm</InputAdornment>}
                                    defaultValue={state.data.height}
                                    onChange={(tar) => setState({
                                        ...state,
                                        data: {
                                            ...state.data,
                                            height: tar.target.value
                                        },
                                        isHeightValid: true,
                                    })}
                                    onBlur={(tar) => setState({
                                        ...state,
                                        data: {
                                            ...state.data,
                                            height: tar.target.value
                                        },
                                        isHeightValid: !tar.target.value.length || valid(tar.target.value, NumberPattern, 'm')
                                    })}
                                    onKeyUp={handleKey}
                                    label="身高"
                                    error={!state.isHeightValid}
                                />
                            </FormControl>
                            {/* 体重 */}
                            <FormControl id="weight_input" sx={{ m: 1, width: '23.2ch', position: "relative" }} variant="outlined">
                                <InputLabel htmlFor="weight" >体重</InputLabel>
                                <OutlinedInput
                                    id="weight"
                                    color="primary"
                                    sx={{ bgcolor: 'white' }}
                                    endAdornment={<InputAdornment position="end">kg</InputAdornment>}
                                    defaultValue={state.data.weight}
                                    onChange={(tar) => setState({
                                            ...state,
                                            data: {
                                                ...state.data,
                                                weight: tar.target.value
                                            },
                                            isWeightValid: true,
                                    })}
                                    onBlur={(tar) => setState({
                                        ...state,
                                        data: {
                                            ...state.data,
                                            weight: tar.target.value
                                        },
                                        isWeightValid: !tar.target.value.length || valid(tar.target.value, NumberPattern, 'm')
                                    })}
                                    onKeyUp={handleKey}
                                    label="体重"
                                    error={!state.isWeightValid}
                                />
                            </FormControl>
                        </Box>
                        <Divider />
                        <Box sx={{ mt: '16px' }}>
                            <Button
                                id="tab2_next"
                                variant="contained"
                                endIcon={<NextPlan />}
                                onClick={nextButtonClick}>下一步</Button>
                        </Box>
                    </TabPanel>
                    <TabPanel value="3" sx={{ width: 675, height: 300 }}>
                        <Box sx={{ pb: '8px' }}>
                            <TextField
                                id="phone_number"
                                label="电话号码"
                                sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                defaultValue={state.data.phoneNumber}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        phoneNumber: tar.target.value
                                    },
                                    isPhoneNumberValid: !tar.target.value.length || valid(tar.target.value, PhoneNumberPattern, 'm')
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        phoneNumber: tar.target.value
                                    },
                                    isPhoneNumberValid: !tar.target.value.length || valid(tar.target.value, PhoneNumberPattern, 'm')
                                })}
                                onKeyUp={handleKey}
                                error={!state.isPhoneNumberValid}
                                placeholder="手机号"
                            />
                        </Box>
                        <Box sx={{ pb: '8px' }}>
                            <TextField
                                id="email"
                                label="邮箱地址"
                                sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                defaultValue={state.data.email}
                                onChange={(tar) => {
                                    setState({
                                        ...state,
                                        data: {
                                            ...state.data,
                                            email: tar.target.value
                                        },
                                        isEmailValid: !tar.target.value.length || valid(tar.target.value, EmailPattern, 'i')
                                    });
                                }}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        email: tar.target.value
                                    },
                                    isEmailValid: !tar.target.value.length || valid(tar.target.value, EmailPattern, 'i')
                                })}
                                onKeyUp={handleKey}
                                error={!state.isEmailValid}
                                placeholder="邮箱地址"
                            />
                        </Box>
                        <Box sx={{ pb: '16px' }}>
                            <TextField
                                id="been_drinking"
                                label="有饮酒习惯？"
                                select
                                sx={{ m: 1, width: '23.2ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={state.data.beenDrinking}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenDrinking: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenDrinking: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}>
                                {AlterList.map((option) => (
                                    <MenuItem key={'drinking-' + option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))
                                }
                            </TextField>
                            <TextField
                                id="been_smoking"
                                label="有吸烟习惯？"
                                select
                                sx={{ m: 1, width: '23.2ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={state.data.beenSmoking}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenSmoking: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenSmoking: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}>
                                {
                                    AlterList.map((option) => (
                                        <MenuItem key={'smoking-' + option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))
                                }
                            </TextField>
                        </Box>
                        <Divider/>
                        <Box sx={{mt: '16px'}}>
                            <Submit onClick={submitButtonClick} />
                        </Box>
                    </TabPanel>
                </TabContext>
            </Box>
        </div>
    );
}