﻿import React, { useState, useRef } from 'react'
import { useNavigate } from 'react-router-dom';
import { Grid, Paper, Box } from '@mui/material';
import { Analytics, PermIdentity, FileUpload} from '@mui/icons-material';
import { green, blue, red } from '@mui/material/colors';
import { CustomerizedIconButton, CustomerizedSwipeableViews } from '../utility/myComponents.jsx';

export default function Home(props) {
    let nav = useNavigate()

    const swipeableRef = useRef(null)

    return (
        <Grid container spacing={2} justifyContent="center" alignItems="center">
            <Box sx={{height: 100, width: '100%'}}></Box>
            <Grid item xs={12} alignItems="center">
                <Paper sx={{ width: 1200, height: 400 }} elevation={16} >
                    <Box justifyContent="center" alignItems="center">
                        <CustomerizedSwipeableViews swipeableRef={swipeableRef} size={5} />
                    </Box>
                </Paper>
            </Grid>
            <Grid item xs={4}>
                <Box sx={{ width: 300, height: 300 }} elevation={16} >
                    <CustomerizedIconButton
                        title="我的报告"
                        onClick={() => nav('/report')}
                        icon={<Analytics sx={{ fontSize: 250, color: green[200] }} />}
                    />
                </Box>
            </Grid>
            <Grid item xs={4}>
                <Box sx={{ width:300, height: 300 }} elevation={16} >
                    <CustomerizedIconButton
                        title="上传文件"
                        onClick={() => nav('/upload')}
                        icon={<FileUpload sx={{ fontSize: 250, color: blue[200] }} />}
                    />
                </Box>
            </Grid>
            <Grid item xs={4}>
                <Box sx={{ width: 300, height: 300 }} elevation={16} >
                    <CustomerizedIconButton
                        title="个人信息"
                        onClick={() => nav('/info')}
                        icon={<PermIdentity sx={{ fontSize: 250, color: red[200] }} />}
                    />
                </Box>
            </Grid>
        </Grid>
    )
}