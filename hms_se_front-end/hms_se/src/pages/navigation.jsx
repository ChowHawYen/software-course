﻿import * as React from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AppBar, Drawer, Box, Toolbar, Typography, Button, IconButton, Divider, InputBase, FormControl, Snackbar, Alert, Dialog, Paper } from '@mui/material';
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import { styled, alpha } from '@mui/material/styles';
import { blue } from '@mui/material/colors';
import { Menu, Home, SearchOutlined, AccountCircle, HelpOutlineRounded, ExpandMore } from '@mui/icons-material';
import { Operations, UserManual } from '../utility/constants.jsx';
import { UserOperation, CustomerizedIconButton } from '../utility/myComponents.jsx';

export default function Navigation(props) {
    //--------------------------------------------------------------------------------
    // hooks
    //--------------------------------------------------------------------------------
    let nav = useNavigate()

    const [menuOpen, setMenuOpen] = useState(false)
    const [helpOpen, setHelpOpen] = useState(false)
    //--------------------------------------------------------------------------------
    // customerized style
    //--------------------------------------------------------------------------------
    const Search = styled('div')(({ theme }) => ({
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    }));

    const SearchIconWrapper = styled('div')(({ theme }) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }));

    const StyledInputBase = styled(InputBase)(({ theme }) => ({
        color: 'inherit',
        '& .MuiInputBase-input': {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)})`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
    }));
    //--------------------------------------------------------------------------------
    // actions
    //--------------------------------------------------------------------------------
    const logout = () => {
        props.logout()
        nav('/login')
    }

    let searchInput = ''

    // 检索
    const search = (e) => {
        if (e.key == 'Enter') {
            console.log(searchInput)
            if (searchInput == '主页' || searchInput == 'home') {
                nav('/home')
                return;
            }

            if (!props.state.login) {
                if (searchInput == '注册' || searchInput == 'register') {
                    nav('/register')
                    return;
                }

                if (searchInput == '找回密码' || searchInput == '忘记密码' || searchInput == 'retrieve') {
                    nav('/retrieve')
                    return;
                }

                nav('/login')
                return;
            } else {
                if (searchInput == '上传文件' || searchInput == 'upload') {
                    nav('/upload')
                    return;
                }

                if (searchInput == '我的报告' || searchInput == 'report') {
                    nav('/report')
                    return;
                }

                if (searchInput == '个人信息' || searchInput == 'info') {
                    nav('/info')
                    return;
                }
            }

        }
    }
    //--------------------------------------------------------------------------------
    // page
    //--------------------------------------------------------------------------------
    return (
        <div>
            <header>
                <Snackbar
                    open={props.state.messageShow}
                    autoHideDuration={3000}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                    onClose={() => props.setMessage("", false, props.state.messageType)}
                    sx={{zIndex: 3000 }}
                >
                    <Alert severity={props.state.messageType} sx={{ width: '100%' }}>
                        {props.state.message}
                    </Alert>
                </Snackbar>
                <Box id="navigation" sx={{ flexGrow: 1, zIndex: 2000 }}>
                    <AppBar position="static" sx={{ bgcolor: '#2e59a7' }}>
                        <Toolbar>
                            <CustomerizedIconButton
                                title="菜单"
                                size="large"
                                edge="start"
                                color="inherit"
                                onClick={() => setMenuOpen((open) => !open)}
                                icon={<Menu />}
                            />
                            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}></Typography>
                            <Search>
                                <SearchIconWrapper>
                                    <SearchOutlined />
                                </SearchIconWrapper>
                                <StyledInputBase
                                    placeholder="Search…"
                                    inputProps={{ 'aria-label': 'search' }}
                                    onChange={(t) => searchInput = t.target.value.toLowerCase()}
                                    onBlur={(t) => {
                                        searchInput = '';
                                        t.target.value = '';
                                    }}
                                    onKeyUp={search}
                                />
                            </Search>
                            {props.state.login ?
                                <Button color="inherit" onClick={logout}>登出</Button> :
                                <Button color="inherit" onClick={() => nav("/login")}>登录</Button>}
                            <CustomerizedIconButton
                                title="帮助"
                                onClick={() => setHelpOpen(true)}
                                color="inherit"
                                size="small"
                                icon={<HelpOutlineRounded />}
                            />
                        </Toolbar>
                    </AppBar>
                </Box>
            </header>
            <Drawer
                anchor='left'
                open={menuOpen}
                onClose={() => setMenuOpen(false)}
                sx={{ zIndex: 2500 }}
            >
                <Box
                    sx={{ width: 250, height: '100%', bgcolor: '#99ccff', zIndex: 2500 }}
                    role="presentation"
                    onClick={() => setMenuOpen(false)}
                >
                    <FormControl sx={{ width: 250, height: 250 }}>
                        <IconButton>
                            <AccountCircle sx={{ width: 100, height: 100 }} />
                        </IconButton>
                        <Box sx={{margin: 'auto'}}>
                            {props.state.login ?
                                <h2 style={{ textAlign: 'center' }}>{ props.state.username }</h2> :
                                <Button color="primary" variant="contained" onClick={() => nav("/login")}>登录</Button>}
                        </Box>
                    </FormControl>
                    <Divider />
                    <List>
                        <ListItem key='home' disablePadding>
                            <ListItemButton onClick={ () => nav('/home') }>
                                <ListItemIcon>
                                    <Home />
                                </ListItemIcon>
                                <ListItemText primary='主页' />
                            </ListItemButton>
                        </ListItem>
                    </List>
                    <Divider />
                    {props.state.login ?
                        <List>
                            {Operations.map((op) => {
                                return <UserOperation
                                    key={op.key}
                                    id={op.key}
                                    text={op.text}
                                    icon={op.icon}
                                    onClick={() => nav('/' + op.key)}
                                />
                            })}
                        </List> : <div />
                    }
                </Box>
            </Drawer>
            <Dialog open={helpOpen} onClose={() => setHelpOpen(false)} sx={{zIndex: 3500}}>
                <Paper sx={{ width: '60ch'}}>
                    {UserManual.map((item, index) => {
                        return (
                            <Accordion key={item.title + index} disableGutters={true}>
                                <AccordionSummary
                                    sx={{ backgroundColor: blue[300] }}
                                    expandIcon={<ExpandMore />}
                                    id={item.title}
                                >
                                    <Typography sx={{ fontFamily: 'fantasy, sans-serif', fontWeight: 'bold', fontSize: 20 }}>{item.title}</Typography>
                                </AccordionSummary>
                                <AccordionDetails sx={{ bgcolor: blue[100] }}>
                                    {
                                        item.details.map((detail, idx) => {
                                            return (
                                                <Accordion key={detail.subtitle + idx} disableGutters={true}>
                                                    <AccordionSummary
                                                        sx={{ backgroundColor: blue[300] }}
                                                        expandIcon={<ExpandMore />}
                                                        id={detail.title}
                                                    >
                                                        <Typography sx={{ fontFamily: 'Impact, sans-serif', fontWeight: 'bold' }}>{detail.subtitle}</Typography>
                                                    </AccordionSummary>
                                                    <AccordionDetails sx={{ bgcolor: blue[100] }}>
                                                        {detail.text.map((paragraph, i) => {
                                                            return <Typography sx={{ m: 0.5, fontFamily: 'Courier New, sans-serif', fontWeight: 'bold' }} key={i} variant="body1" paragraph={true}>{paragraph}</Typography>
                                                        })}
                                                    </AccordionDetails>
                                                </Accordion>
                                            )
                                        })
                                    }
                                </AccordionDetails>
                            </Accordion>
                        )
                    })}
                </Paper>
            </Dialog>
        </div>
    );
}