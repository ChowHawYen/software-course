﻿import React, { useState, useEffect } from 'react'
import { useNavigate, Link } from 'react-router-dom';
import { FormControl, OutlinedInput, Collapse, InputLabel, InputAdornment, IconButton, Box } from '@mui/material'
import { ArrowCircleDownTwoTone, ArrowCircleUpTwoTone, ArrowCircleRightTwoTone, Visibility, VisibilityOff } from '@mui/icons-material'
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import axios from 'axios';
import $ from 'jquery';
import { LoginURL, option } from '../utility/constants.jsx';

export default function Login(props) {
    let nav = useNavigate()

    const theme = createTheme({
        typography: {
            tips: {
                fontSize: 13,
            }
        }
    });
    //--------------------------------------------------------------------------------
    // hooks
    //--------------------------------------------------------------------------------
    const [state, setState] = useState({
        userId: '',
        password: '',
        isButtonEnable: false,
        isClicked: false
    });
    const [showPassword, setShowPassword] = useState(false);

    useEffect(() => {
        if (props.state.login) {
            nav('/home')
            return;
        }

        $('#user_id').focus()
    }, [])
    //--------------------------------------------------------------------------------
    // actions
    //--------------------------------------------------------------------------------
    // 按键
    const handleKey = (e) => {
        // 回车键
        if (e.key == 'Enter') {
            if (e.target.id == "user_id") {
                setState({
                    ...state,
                    isClicked: state.isButtonEnable ? true : false
                });
                setTimeout(() => $("#user_password").focus(), 10)
            } else if (e.target.id == "user_password") {
                submitUserInfo()
            }
        }
    };

    // 用户登录
    const submitUserInfo = async () => {
        if (state.userId.length != 0 && state.password != 0) {

            const userInfo = {
                username: state.userId,
                password: state.password,
            }

            axios.post(LoginURL, userInfo, option)
                .then(res => {
                    if (res.status == 200) {
                        if (res.data.status == "success") {
                            const data = res.data.content
                            props.login(state.userId, data.name)
                            nav('/home')
                        } else {
                            props.setMessage(res.data.message, true, "warning")
                        }
                    }
                })
                .catch(err => {
                    props.setMessage(err.message, true, "error")
                })
        }
    }

    //--------------------------------------------------------------------------------
    // page
    //--------------------------------------------------------------------------------
    return (
        <Box>
            <div>
                {/* 用户ID输入模块 */}
                <FormControl sx={{ m: 0.5, width: '35ch' }} variant="outlined">
                    <InputLabel>用户ID</InputLabel>
                    {/* 输入框 */ }
                    <OutlinedInput
                        id="user_id"
                        color="primary"
                        defaultValue={state.userId}
                        sx={{bgcolor: 'white'}}
                        onChange={(e) => setState({
                            ...state,
                            userId: e.target.value,
                            isButtonEnable: e.target.value.length > 0 ? true : false,
                            isClicked: e.target.value.length > 0 ? state.isClicked : false
                        })}
                        onKeyUp={handleKey}
                        endAdornment={
                            <InputAdornment position="end">
                                { /* 图标 */ }
                                <IconButton
                                    id="next_button"
                                    variant="outlined"
                                    edge="end"
                                    onClick={() => setState({
                                        ...state,
                                        isClicked: state.isButtonEnable ? !state.isClicked : false
                                    })}
                                >
                                    {
                                        state.isClicked ?
                                            <ArrowCircleUpTwoTone fontSize="large" color="primary" /> :
                                            <ArrowCircleDownTwoTone fontSize="large" color={state.isButtonEnable ? "primary" : "disabled"} disabled={!state.isButtonEnable} />
                                    }
                                </IconButton>
                            </InputAdornment>
                        }
                        label="用户ID"
                        placeholder="手机号/邮箱地址"
                    />
                </FormControl>
            </div>
            <div>
                {/* 是否显示密码输入模块 */}
                <Collapse in={state.isClicked} >
                    <FormControl id="password_input" sx={{ m: 0.5, width: '35ch', bgcolor: 'white' }} variant="outlined">
                        <InputLabel htmlFor="user_password" >密码</InputLabel>
                        <OutlinedInput
                            id="user_password"
                            type={showPassword ? 'text' : 'password'}
                            color="primary"
                            defaultValue={state.password}
                            onKeyUp={handleKey}
                            onChange={(e) => setState({
                                ...state,
                                password: e.target.value
                            })}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={() => setShowPassword((show) => (!show))}
                                        onMouseDown={(event) => { event.preventDefault() }}
                                        edge="end"
                                    >
                                        {/* 显示密码 */}
                                        {showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                    <IconButton
                                        id="confirm_button"
                                        aria-label="toggle password visibility"
                                        variant="outlined"
                                        edge="end"
                                        onClick={submitUserInfo}
                                        onMouseDown={(event) => { event.preventDefault() }}
                                    >
                                        <ArrowCircleRightTwoTone fontSize="large" color="primary" />
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="密码"
                            placeholder="密码"
                        />
                    </FormControl>
                </Collapse>
            </div>
            <div>
                <ThemeProvider theme={theme}>
                    <Typography variant="tips">
                        <div>
                            没有账号？<Link to='/register'>注册</Link>
                        </div>
                    </Typography>
                    <Typography variant="tips">
                        <div>
                            <Link to='/retrieve'>忘记密码？</Link>
                        </div>
                    </Typography>
                </ThemeProvider>
            </div>
        </Box>)
}

