﻿import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import { FormControl, OutlinedInput, InputLabel, Button, TextField, MenuItem, InputAdornment, FormGroup, Box, Divider } from '@mui/material'
import { ManageAccounts } from '@mui/icons-material';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import customParseFormat from 'dayjs/plugin/customParseFormat'
import dayjs from 'dayjs'
import { valid, format } from '../utility/tool.jsx'
import { GenderList, AlterList, option, SearchURL, ModifyURL, EmailPattern, PhoneNumberPattern, NumberPattern, NonNumeric, UncorrectPhoneNumber, UncorrectEmail, ModifySuccess } from '../utility/constants.jsx'
import { Submit, Loading } from '../utility/myComponents.jsx';
import $ from 'jquery';
import axios from 'axios';

export default function Info(props) {
    //--------------------------------------------------------------------------------
    // hooks
    //--------------------------------------------------------------------------------
    // router
    let nav = useNavigate()
    // state
    const [state, setState] = React.useState({
        data: undefined,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isHeightValid: true,
        isWeightValid: true,
        isPhoneNumberValid: true,
        isEmailValid: true,
    })
    const [change, setChange] = useState(false)
    useEffect(() => {
        if (!props.state.login) {
            nav('/login')
            return;
        }

        const getUserInfo = async () => {
            axios.get(SearchURL, {
                params: {
                    username: props.state.username
                }
            })
                .then(res => {
                    if (res.status == 200) {
                        if (res.data.status == "success") {
                            const data = res.data.content
                            dayjs.extend(customParseFormat)
                            setState({
                                ...state,
                                data: {
                                    name: data.name,
                                    gender: data.gender,
                                    birthday: dayjs(data.birthday) ,
                                    height: data.height,
                                    weight: data.weight,
                                    phoneNumber: data.phone_number,
                                    email: data.email,
                                    beenDrinking: Boolean(data.beenDrinking),
                                    beenSmoking: Boolean(data.beenSmoking),
                                },
                            })
                        } else {
                            props.setMessage(res.data.message, true, "warning")
                        }
                    }
                })
                .catch(err => {
                    props.setMessage(err.message, true, "error")
                })
        }
        getUserInfo()
    }, [])

    //--------------------------------------------------------------------------------
    // actions
    //--------------------------------------------------------------------------------
    const handleKey = (e) => {
        if (e.key == 'Enter') {
            switch (e.target.id) {
                case 'user_name': $("#gender").focus(); break;
                case 'height': $("#weight").focus(); break;
                case 'weight': $("#phone_number").focus(); break;
                case 'phone_number': $('#email').focus(); break;
                case 'email': submitButtonClick(); break;
                default: break;
            }

        }
    }

    // 提交按钮
    const submitButtonClick = async () => {
        // 检查输入格式是否正确
        if (!valid(state.data.height, NumberPattern, 'm', true)) { props.setMessage(format(NonNumeric, '身高'), true, "warning"); return; }
        if (!valid(state.data.weight, NumberPattern, 'm', true)) { props.setMessage(format(NonNumeric, '体重'), true, "warning"); return; }
        if (!valid(state.data.phoneNumber, PhoneNumberPattern, 'm', true)) { props.setMessage(UncorrectPhoneNumber, true, "warning"); return; }
        if (!valid(state.data.email, EmailPattern, 'm', true)) { props.setMessage(UncorrectEmail, true, "warning"); return; }

        const info = {
            username: props.state.username,
            name: state.data.name,
            gender: state.data.gender,
            birthday: state.data.birthday.format("YYYY-MM-DD"),
            height: state.data.height,
            weight: state.data.weight,
            phone_number: state.data.phoneNumber,
            email: state.data.email,
            been_drinking: state.data.beenDrinking,
            been_smoking: state.data.beenSmoking
        }

        axios.post(ModifyURL, info, option)
            .then(res => {
                if (res.status == 200) {
                    if (res.data.status == "success") {
                        props.setMessage(format(ModifySuccess, '个人信息'), true, "success")
                        setChange(false)
                    } else {
                        props.setMessage(res.data.message, true, "warning")
                    }
                }
            })
            .catch(err => {
                props.setMessage(err.message, true, "error")
            })
    }

    if (state.data == undefined) {
        return (
            <Loading />
        )
    }

    return (
        <div>
            <Box sx={{ display: 'flex', flexWrap: 'nowrap' }}>
                {/* 姓名 */}
                <TextField
                    id="name"
                    label="姓名"
                    sx={{ m: 1, width: '35ch', bgcolor: 'white' }}
                    color="primary"
                    variant="outlined"
                    defaultValue={state.data.name}
                    onChange={(tar) => setState({
                        ...state,
                        data: {
                            ...state.data,
                            name: tar.target.value
                        }
                    })}
                    onBlur={(tar) => setState({
                        ...state,
                        data: {
                            ...state.data,
                            name: tar.target.value
                        }
                    })}
                    onFocus={() => console.log(state)}
                    onKeyUp={handleKey}
                    disabled={!change}
                />
                {/* 性别 */}
                {change ?
                    <TextField
                        id="gender"
                        label="性别"
                        select
                        sx={{ m: 1, width: '11.4ch', bgcolor: 'white' }}
                        color="primary"
                        variant="outlined"
                        value={state.data.gender}
                        onChange={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                gender: tar.target.value
                            }
                        })}
                        onBlur={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                gender: tar.target.value
                            }
                        })}
                        onKeyUp={handleKey}>
                        {GenderList.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))
                        }
                    </TextField> :
                    <TextField
                        id="gender"
                        label="性别"
                        sx={{ m: 1, width: '11.4ch', bgcolor: 'white' }}
                        color="primary"
                        variant="outlined"
                        value={GenderList.filter((e) => e.value === state.data.gender)[0].label}
                        onChange={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                gender: tar.target.value
                            }
                        })}
                        onBlur={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                gender: tar.target.value
                            }
                        })}
                        onKeyUp={handleKey}
                        disabled={true}
                    >
                    </TextField>
                }
            </Box>
            {/* 生日 */}
            <FormControl id="birthday_input" sx={{ m: 1, pb: '8px', width: '48ch', position: "relative" }} variant="outlined" onKeyUp={(e) => { if (e.key == 'Enter') $('#height').focus() }}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={['DatePicker']}>
                        <DatePicker
                            id="birthday"
                            sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                            color="primary"
                            label="出生日期"
                            defaultValue={state.data.birthday}
                            onChange={(tar) => setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    birthday: tar.format('YYYY/MM/DD')
                                }
                            })}
                            onBlur={(tar) => setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    birthday: tar.format('YYYY/MM/DD')
                                }
                            })}
                            onKeyUp={handleKey}
                            disableFuture
                            disabled={!change}
                        />
                    </DemoContainer>
                </LocalizationProvider>
            </FormControl>
            <Box sx={{pb: '8px'}}>
                {/* 身高 */}
                <FormControl id="height_input" sx={{ m: 1, width: '23.2ch', position: "relative" }} variant="outlined">
                    <InputLabel htmlFor="height" >身高</InputLabel>
                    <OutlinedInput
                        id="height"
                        color="primary"
                        sx={{ bgcolor: 'white' }}
                        endAdornment={<InputAdornment position="end">cm</InputAdornment>}
                        defaultValue={state.data.height}
                        onChange={(tar) => setState({
                            ...state,
                            isHeightValid: true,
                            data: {
                                ...state.data,
                                height: tar.target.value
                            }
                        })}
                        onBlur={(tar) => setState({
                            ...state,
                            isHeightValid: !tar.target.value.length || valid(tar.target.value, NumberPattern, 'm'),
                            data: {
                                ...state.data,
                                height: tar.target.value
                            }
                        })}
                        onKeyUp={handleKey}
                        label="身高"
                        error={!state.isHeightValid}
                        disabled={!change}
                    />
                </FormControl>
                {/* 体重 */}
                <FormControl id="weight_input" sx={{ m: 1, width: '23.2ch', position: "relative" }} variant="outlined">
                    <InputLabel htmlFor="weight" >体重</InputLabel>
                    <OutlinedInput
                        id="weight"
                        color="primary"
                        sx={{ bgcolor: 'white' }}
                        endAdornment={<InputAdornment position="end">kg</InputAdornment>}
                        defaultValue={state.data.weight}
                        onChange={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                weight: tar.target.value
                            },
                            isWeightValid: true,
                        })}
                        onBlur={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                weight: tar.target.value
                            },
                            isWeightValid: !tar.target.value.length || valid(tar.target.value, NumberPattern, 'm'),
                        })}
                        onKeyUp={handleKey}
                        label="体重"
                        error={!state.isWeightValid}
                        disabled={!change}
                    />
                </FormControl>
            </Box>
            <Box sx={{ pb: '8px' }}>
                <TextField
                    id="phone_number"
                    label="电话号码"
                    sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                    color="primary"
                    variant="outlined"
                    defaultValue={state.data.phoneNumber}
                    onChange={(tar) => setState({
                        ...state,
                        data: {
                            ...state.data,
                            phoneNumber: tar.target.value
                        },
                        isPhoneNumberValid: !tar.target.value.length || valid(tar.target.value, PhoneNumberPattern, 'm')
                    })}
                    onBlur={(tar) => setState({
                        ...state,
                        data: {
                            ...state.data,
                            phoneNumber: tar.target.value
                        },
                        isPhoneNumberValid: !tar.target.value.length || valid(tar.target.value, PhoneNumberPattern, 'm')
                    })}
                    onKeyUp={handleKey}
                    error={!state.isPhoneNumberValid}
                    placeholder="手机号"
                    disabled={!change}
                />
            </Box>
            <Box sx={{ pb: '8px' }}>
                <TextField
                    id="email"
                    label="邮箱地址"
                    sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                    color="primary"
                    variant="outlined"
                    defaultValue={state.data.email}
                    onChange={(tar) => setState({
                        ...state,
                        data: {
                            ...state.data,
                            email: tar.target.value
                        },
                        isEmailValid: !tar.target.value.length || valid(tar.target.value, EmailPattern, 'i')
                    })}
                    onBlur={(tar) => setState({
                        ...state,
                        data: {
                            ...state.data,
                            email: tar.target.value
                        },
                        isEmailValid: !tar.target.value.length || valid(tar.target.value, EmailPattern, 'i')
                    })}
                    onKeyUp={handleKey}
                    error={!state.isEmailValid}
                    placeholder="邮箱地址"
                    disabled={!change}
                />
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'space-around', pb: '8px' }}>
                <FormControl component="fieldset">
                    {change ?
                        <FormGroup row>
                            <TextField
                                id="been_drinking"
                                label="有饮酒习惯？"
                                select
                                sx={{ m: 1, width: '23.2ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={state.data.beenDrinking}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenDrinking: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenDrinking: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}>
                                {AlterList.map((option) => (
                                    <MenuItem key={'drinking-' + option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))
                                }
                            </TextField>
                            <TextField
                                id="been_smoking"
                                label="有吸烟习惯？"
                                select
                                sx={{ m: 1, width: '23.2ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={state.data.beenSmoking}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenSmoking: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenSmoking: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}>
                                {
                                    AlterList.map((option) => (
                                        <MenuItem key={'smoking-' + option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))
                                }
                            </TextField>
                        </FormGroup>:
                        <FormGroup row>
                            <TextField
                                id="been_drinking"
                                label="有饮酒习惯？"
                                sx={{ m: 1, width: '23.2ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={AlterList.filter((e) => e.value === state.data.beenDrinking)[0].label}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenDrinking: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenDrinking: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}
                                disabled={true}
                            />
                            <TextField
                                id="been_smoking"
                                label="有吸烟习惯？"
                                sx={{ m: 1, width: '23.2ch', bgcolor: 'white' }}
                                color="primary"
                                variant="outlined"
                                value={AlterList.filter((e) => e.value === state.data.beenSmoking)[0].label}
                                onChange={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenSmoking: tar.target.value
                                    }
                                })}
                                onBlur={(tar) => setState({
                                    ...state,
                                    data: {
                                        ...state.data,
                                        beenSmoking: tar.target.value
                                    }
                                })}
                                onKeyUp={handleKey}
                                disabled={true}
                            />
                        </FormGroup>
                    }
                </FormControl>
            </Box>
            <Divider />
            <Box sx={{ mt: '16px' }}>
                {change ? 
                    <Submit onClick={submitButtonClick} /> :
                    <Button
                        variant="contained"
                        endIcon={<ManageAccounts />}
                        onClick={() => setChange(true)}
                    >修　改</Button>
                    }
            </Box>
        </div>
    );
}