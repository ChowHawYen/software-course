﻿import React, { useState, useEffect } from 'react';
import dayjs from 'dayjs';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Grid, Paper, Box, CircularProgress, Typography } from '@mui/material';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import { QueryStats, ExpandMore } from '@mui/icons-material';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { LineChart, PieChart, BarChart, ChartsYAxis } from '@mui/x-charts';
import { ReportURL, option } from '../utility/constants.jsx';
import { CustomerizedIconButton, HiddenLine, CustomerizedRating } from '../utility/myComponents.jsx'
import { min, max, isSameHour } from '../utility/tool.jsx'
import customParseFormat from 'dayjs/plugin/customParseFormat'

export default function Report(props) {
    let nav = useNavigate();

    const [data, setData] = useState([])

    const [date, setDate] = useState({
        from: dayjs(),
        to: dayjs()
    })

    const [dateRange, setDateRange] = useState({
        minDate: null,
        maxDate: null
    })

    useEffect(() => {
        if (!props.state.login) {
            nav('/login')
            return;
        }

        const getUserInfo = async () => {
            axios.get(ReportURL, {
                params: {
                    username: props.state.username
                }
            })
                .then(res => {
                    if (res.status == 200) {
                        if (res.data.status == "success") {
                            const data = res.data.content
                            dayjs.extend(customParseFormat)
                            let min = dayjs(data.minDate)
                            let max = dayjs(data.maxDate)
                            setDateRange({
                                minDate: min,
                                maxDate: max
                            })
                            setDate({
                                from: min,
                                to: min
                            })
                        } else {
                            props.setMessage(res.data.message, true, "warning")
                        }
                    }
                })
                .catch(err => {
                    props.setMessage(err.message, true, "error")
                })
        }
        getUserInfo()
    },[])

    const getReport = async () => {
        setData(undefined)
        const query = {
            username: props.state.username,
            startTime: date.from.format("YYYY-MM-DD"),
            endTime: date.to.format("YYYY-MM-DD"),
        }

        axios.post(ReportURL, query, option)
            .then(res => {
                if (res.status == 200) {
                    if (res.data.status == "success") {
                        let results = res.data.content
                        let allData = results.map((result) => {

                            let sum = 0
                            let cnt = 0
                            let x = [];
                            let y = [];
                            let label = (new Date(result.heart_rate[1][0]));
                            for (let i = 0; i < result.heart_rate[1].length; i++) {
                                let tmp = (new Date(result.heart_rate[1][i]));
                                sum += result.heart_rate[0][i];
                                cnt++;
                                if (!isSameHour(label, tmp)) {
                                    x.push(label.getHours() + ':00');
                                    y.push((sum / cnt).toFixed(1));
                                    label = tmp;
                                    sum = 0;
                                    cnt = 0;
                                    continue;
                                }
                            }
                            x.push(label.getHours() + ':00');
                            y.push((sum / cnt).toFixed(1));

                            return {
                                label: label.getFullYear() + '/' + label.getMonth() + '/' + label.getDate(),
                                activity: {
                                    ...result.activity,
                                    score: result.activity.score / 20
                                },
                                sleep: {
                                    ...result.sleep,
                                    score: result.sleep.score / 20,
                                    shallow_sleep_time: result.shallow_sleep_time / 1000,
                                    deep_sleep_time: result.deep_sleep_time / 1000,
                                    dream_sleep_time: result.dream_sleep_time / 1000,
                                    noontime: result.noontime / 1000
                                },
                                stress: {
                                    ...result.stress,
                                    score: (100 - result.stress.score) / 20
                                },
                                time_graph: {
                                    x: result.time_graph[1].map((e) => {
                                        let d = new Date(e);
                                        return (d.getHours() + ':' + d.getMinutes())
                                    }),
                                    y: result.time_graph[0]
                                },
                                heart_rate: {
                                    x: x,
                                    y: y
                                }
                            }
                        })
                        console.log(allData)
                        setData(allData)
                    } else {
                        props.setMessage(res.data.message, true, "warning")
                    }
                }
            })
            .catch(err => {
                props.setMessage(err.message, true, "error")
            })
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Grid container spacing={0}>
                    <Grid item xs={5}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DemoContainer components={['DateRangePicker', 'DateRangePicker']}>
                                <DatePicker
                                    id="from"
                                    sx={{ m: 1, bgcolor: 'white' }}
                                    color="primary"
                                    label="开始日期"
                                    value={date.from}
                                    onChange={(tar) => setDate({
                                            from: tar,
                                            to: max(tar, date.to)
                                        })
                                    }
                                    minDate={dateRange.minDate}
                                    maxDate={dateRange.maxDate}
                                />
                            </DemoContainer>
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={1}>
                        <Box sx={{ m: 0.5, mt: 1, fontSize: 34, fontWeight: 'bold' }}>-</Box>
                    </Grid>
                    <Grid item xs={5}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DemoContainer components={['DateRangePicker', 'DateRangePicker']}>
                                <DatePicker
                                    id="to"
                                    sx={{ m: 1, bgcolor: 'white' }}
                                    color="primary"
                                    label="结束日期"
                                    value={date.from}
                                    onChange={(tar) => setDate({
                                        from: min(tar, date.from),
                                        to: tar
                                    })}
                                    minDate={dateRange.minDate}
                                    maxDate={dateRange.maxDate}
                                />
                            </DemoContainer>
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={1}>
                        <Box sx={{ mt: 2 }}>
                            <CustomerizedIconButton
                                title="查看报告"
                                onClick={getReport}
                                icon={<QueryStats />}
                            />
                        </Box>
                    </Grid>
                </Grid>
                <HiddenLine height={50} />
                <Grid container spacing={0}>
                    <Grid item xs={12}>
                        <Box sx={{ mb: '16px', width: 600, height: 400 }}>
                            {
                                data == undefined || data.length == 0 ?
                                    <Grid container direction="column" spacing={6}>
                                        <Grid item xs={2} />
                                        <Grid item xs={8}>
                                            <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                                                <CircularProgress />
                                            </Box>
                                        </Grid>
                                        <Grid item xs={2} />
                                        <Grid item xs={12}></Grid>
                                    </Grid> :
                                    data.map((day, index) => {
                                        return (
                                            < Box sx={{ width: '100%' }}>
                                                <Accordion disableGutters={true}>
                                                    <AccordionSummary
                                                        sx={{ backgroundColor: '#00bc12' }}
                                                        expandIcon={<ExpandMore />}
                                                        id='sleep'
                                                    >
                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>{day.label}</Typography>
                                                    </AccordionSummary>
                                                    <AccordionDetails sx={{ bgcolor: '#66ff66' }}>
                                                        <Accordion disableGutters={true}>
                                                            <AccordionSummary
                                                                sx={{ backgroundColor: '#00bc12' }}
                                                                expandIcon={<ExpandMore />}
                                                                id='sleep'
                                                            >
                                                                <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>睡眠</Typography>
                                                            </AccordionSummary>
                                                            <AccordionDetails sx={{ bgcolor: '#66ff66' }}>
                                                                <Grid container spacing={2}>
                                                                    <Grid item xs={4}>
                                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>评分：</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={8}>
                                                                        <CustomerizedRating value={day.sleep.score}></CustomerizedRating>
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                                                                            <PieChart
                                                                                series={[
                                                                                    {
                                                                                        data: [
                                                                                            { id: 0, value: day.sleep.shallow_sleep_time, label: '浅睡眠' },
                                                                                            { id: 1, value: day.sleep.deep_sleep_time, label: '深度睡眠' },
                                                                                            { id: 2, value: day.sleep.dream_sleep_time, label: '梦眠' },
                                                                                            { id: 3, value: day.sleep.noontime, label: '午睡' },
                                                                                        ],
                                                                                    },
                                                                                ]}
                                                                                width={400}
                                                                                height={200}
                                                                            />
                                                                        </Box>
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20, textAlign: 'left' }}>
                                                                            {'　　' + day.sleep.advice}
                                                                        </Typography>
                                                                    </Grid>
                                                                </Grid>
                                                            </AccordionDetails>
                                                        </Accordion>
                                                        <Accordion disableGutters={true}>
                                                            <AccordionSummary
                                                                sx={{ backgroundColor: '#00bc12' }}
                                                                expandIcon={<ExpandMore />}
                                                                id='activity'
                                                            >
                                                                <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>运动</Typography>
                                                            </AccordionSummary>
                                                            <AccordionDetails sx={{ bgcolor: '#66ff66' }}>
                                                                <Grid container spacing={2}>
                                                                    <Grid item xs={4}>
                                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>评分：</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={8}>
                                                                        <CustomerizedRating value={day.activity.score}></CustomerizedRating>
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                                                                            <BarChart
                                                                                xAxis={[{ scaleType: 'band', data: day.heart_rate.x }]}
                                                                                series={[
                                                                                    {
                                                                                        label: '平均心率（次/分）',
                                                                                        data: day.heart_rate.y,
                                                                                    },
                                                                                ]}
                                                                                width={500}
                                                                                height={300}
                                                                            />
                                                                        </Box>
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20, textAlign: 'left' }}>{'　　' + day.activity.advice}</Typography>
                                                                    </Grid>
                                                                </Grid>
                                                            </AccordionDetails>
                                                        </Accordion>
                                                        <Accordion disableGutters={true}>
                                                            <AccordionSummary
                                                                sx={{ backgroundColor: '#00bc12' }}
                                                                expandIcon={<ExpandMore />}
                                                                id='stress'
                                                            >
                                                                <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>压力</Typography>
                                                            </AccordionSummary>
                                                            <AccordionDetails sx={{ bgcolor: '#66ff66' }}>
                                                                <Grid container spacing={2}>
                                                                    <Grid item xs={4}>
                                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>评分：</Typography>
                                                                    </Grid>
                                                                    <Grid item xs={8}>
                                                                        <CustomerizedRating value={day.stress.score}></CustomerizedRating>
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20, textAlign: 'left' }}>{'　　' + day.stress.advice}</Typography>
                                                                    </Grid>
                                                                </Grid>
                                                            </AccordionDetails>
                                                        </Accordion>
                                                        <Accordion disableGutters={true}>
                                                            <AccordionSummary
                                                                sx={{ backgroundColor: '#00bc12' }}
                                                                expandIcon={<ExpandMore />}
                                                                id='stress'
                                                            >
                                                                <Typography sx={{ fontFamily: 'PingFang SC, Microsoft YaHei', fontWeight: 'bold', fontSize: 20 }}>睡眠记录</Typography>
                                                            </AccordionSummary>
                                                            <AccordionDetails sx={{ bgcolor: '#66ff66' }}>
                                                                <Grid container spacing={2}>
                                                                    <Grid item xs={12}>
                                                                        <Box>
                                                                            <LineChart
                                                                                xAxis={[{ data: day.time_graph.x, scaleType: 'point' }]}
                                                                                series={[{ data: day.time_graph.y, showMark: false, area: true }]}
                                                                                yAxis={[{ min: 89, max: 93 }]}
                                                                                height={200}
                                                                                margin={{ top: 10, bottom: 20 }}
                                                                            />
                                                                        </Box>
                                                                    </Grid>
                                                                </Grid>
                                                            </AccordionDetails>
                                                        </Accordion>
                                                    </AccordionDetails>
                                                </Accordion>
                                            </Box>
                                        )
                                    })
                            }
                        </Box>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}