﻿import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Box } from '@mui/material';
import { UploadURL, fileOption } from '../utility/constants.jsx';
import { TransferList } from '../utility/myComponents.jsx';

export default function Uploaad(props) {
    let nav = useNavigate();

    const [state, setState] = useState({
        fileList: [],
        uploadList: [],
    })

    const [status, setStatus] = useState(new Map())
    const [files, setFiles] = useState(new Map())

    useEffect(() => {
        if (!props.state.login) {
            nav('/login')
            return;
        }
    }, [])

    const uploadFile = (newFileList, newUploadList) => {
        setState({
            fileList: newFileList,
            uploadList: newUploadList
        })
        submitFiles(newUploadList);
    }

    const retry = (newFileList, newUploadList, target) => {
        setState({
            fileList: newFileList,
            uploadList: newUploadList
        })
        setStatus(new Map(status.set(target, 'waiting')))
    }

    const selectFile = (e) => {
        for (const file of e.target.files) {
            if (!files.has(file.name)) {
                setState({
                    ...state,
                    fileList: [...state.fileList, file.name]
                })
                setStatus(new Map(status.set(file.name, 'waiting')))
                setFiles(new Map(files.set(file.name, file)))
            } else if (status.get(file.name) == 'failure') {
                setState({
                    fileList: [...state.fileList, file.name],
                    uploadList: [...state.uploadList].filter((e) => e !== file.name),
                })
                setStatus(new Map(status.set(file.name, 'waiting')))
            }
        }
    }

    const submitFiles = async (targets) => {
        targets.map((k) => {
            if (status.get(k) == 'waiting') {
                const formData = new FormData();
                formData.append('username', props.state.username);
                formData.append('file', files.get(k));
                setStatus(new Map(status.set(k, 'processing')))
                axios.post(UploadURL, formData, fileOption)
                    .then(res => {
                        if (res.status == 200) {
                            if (res.data.status == "success") {
                                setStatus(new Map(status.set(k, 'success')))
                            } else {
                                props.setMessage(res.data.message, true, "warning")
                            }
                        }
                    })
                    .catch(err => {
                        setStatus(new Map(status.set(k, 'failure')))
                        props.setMessage(err.message, true, "error")
                    })
            }
        })
    }

    return (
        <Box>
            <Box sx={{mb: "16px"}}>
                <TransferList
                    fileList={state.fileList}
                    uploadList={state.uploadList}
                    selectFile={selectFile}
                    uploadFile={uploadFile}
                    retry={retry}
                    status={status} />
            </Box>
        </Box>

    )
}