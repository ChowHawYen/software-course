﻿import React, { useState, useEffect, useRef } from 'react'
import { useNavigate } from 'react-router-dom';
import { Box, TextField, Divider, Button, Collapse } from '@mui/material'
import axios from 'axios';
import $ from 'jquery';
import { valid, format } from '../utility/tool.jsx'
import { Password, Submit } from '../utility/myComponents.jsx';
import { RetrieveURL, RetrieveVarifyURL, option, PasswordPattern, InCompleteInput, UncorretPassword, UnmatchedPassword, ConfirmCodeSent, ModifySuccess } from '../utility/constants.jsx';

export default function Retrieve(props) {
    // 计时器参数
    const [count, setCount] = useState(120);
    const timerRef = useRef(null);

    timerRef.current = () => {
        setCount(c => c - 1);
    }

    useEffect(() => {
        if (props.state.login) {
            nav('/home')
            return;
        }

        $('#user_id').focus()
    }, [])
    //--------------------------------------------------------------------------------
    // hooks
    //--------------------------------------------------------------------------------
    // router
    let nav = useNavigate()
    // state
    const [state, setState] = useState({
        data: {
            username: '',
            password: '',
            confirmPassword: '',
            confirmCode: '',
        },
        isPasswordValid: true,
        isConfirmPasswordValid: true,
    })
    const [isConfirmCodeGotten, setIsConfirmCodeGotten] = useState(false)
    // 计时器
    const [timer, setTimer] = useState(null)
    // show password
    const [showPassword, setShowPassword] = useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = useState(false)

    //--------------------------------------------------------------------------------
    // actions
    //--------------------------------------------------------------------------------
    //回车键
    const handleKey = (e) => {
        if (e.key == 'Enter') {
            switch (e.target.id) {
                case 'user_id': $("#confirm_code").focus(); break;
                case 'confirm_code': $("#confirm_code_get").focus(); break;
                case 'confirm_code_get': getConfirmCodeButtonClick(); break;
                case 'user_password': $("#confirm_password").focus(); break;
                case 'confirm_password': submitButtonClick(); break;
                default: break;
            }

        }
    }

    // 显示密码按钮
    const handlePasswordShowClick = (callback, target) => {
        callback()
        $('#' + target).focus()
    }

    // 获取验证码
    const getConfirmCodeButtonClick = async () => {
        // 检查输入是否完整
        if (state.data.username.length == 0) { props.setMessage(format(InCompleteInput, '用户名'), true, "warning"); return; }
        axios.get(RetrieveURL, {
            params: {
                username: state.data.username
            }
        })
            .then(res => {
                if (res.status == 200) {
                    if (res.data.status == "success") {
                        props.setMessage(format(ConfirmCodeSent, res.data.content.email), true, "success")
                        let timerId = setInterval(() => {
                            timerRef.current();
                        }, 1000);
                        setTimer(timerId)
                        setIsConfirmCodeGotten(true)
                        setTimeout(() => $("#confirm_code").focus(), 10)
                    } else {
                        props.setMessage(res.data.message, true, "warning")
                    }
                }
            })
            .catch(err => {
                props.setMessage(err.message, true, "error")
            })
    }

    // 提交按钮
    const submitButtonClick = async () => {
        // 检查输入是否完整
        if (state.data.password.length == 0) { props.setMessage(format(InCompleteInput, '密码'), true, "warning"); return; }
        if (state.data.confirmPassword.length == 0) { props.setMessage(format(InCompleteInput, '密码确认'), true, "warning"); return; }

        // 检查输入格式是否正确
        if (!valid(state.data.password, PasswordPattern, 'g')) { props.setMessage(UncorretPassword, true, "warning"); return; }

        // 检查密码确认
        if (state.data.password != state.data.confirmPassword) { props.setMessage(UnmatchedPassword, true, "warning"); return; }

        const userInfo = {
            username: state.data.username,
            new_password: state.data.password,
            code: state.data.confirmCode,
        }

        axios.post(RetrieveVarifyURL, userInfo, option)
            .then(res => {
                if (res.status == 200) {
                    if (res.data.status == "success") {
                        props.setMessage(format(ModifySuccess, '密码'), true, "success")
                        nav("/login")
                    } else {
                        props.setMessage(res.data.message, true, "warning")
                    }
                }
            })
            .catch(err => {
                props.setMessage(err.message, true, "error")
            })
    }

    if (count <= 0) {
        setCount(120)
        setIsConfirmCodeGotten(false)
        clearInterval(timer)
    }

    //--------------------------------------------------------------------------------
    // page
    //--------------------------------------------------------------------------------
    return (
        <div>
            <Box sx={{ display: 'flex', flexWrap: 'nowrap', width: 675, height: 300, justifyContent: 'center', flexDirection: 'column' }}>
                {/* 用户ID */}
                <Box sx={{ pb: '8px' }}>
                    <TextField
                        id="user_id"
                        label="用户ID"
                        sx={{ m: 1, width: '48ch', bgcolor: 'white' }}
                        color="primary"
                        variant="outlined"
                        defaultValue={state.data.username}
                        onChange={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                username: tar.target.value
                            }
                        })}
                        onBlur={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                username: tar.target.value
                            }
                        })}
                        onKeyUp={handleKey}
                        placeholder="用户名"
                    />
                </Box>
                <Box sx={{ display: 'flex', pb: '8px', justifyContent: 'center', alignItems: 'center' }}>
                    <TextField
                        id='confirm_code'
                        label="验证码"
                        sx={{ m: 1, width: '35ch', bgcolor: 'white' }}
                        color="primary"
                        variant="outlined"
                        onChange={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                confirmCode: tar.target.value
                            }
                        })}
                        onBlur={(tar) => setState({
                            ...state,
                            data: {
                                ...state.data,
                                confirmCode: tar.target.value
                            }
                        })}
                        onKeyUp={handleKey}
                        placeholder="验证码"
                    >
                    </TextField>
                    <Button
                        id='confirm_code_get'
                        sx={{ width: '15.5ch' }}
                        onClick={getConfirmCodeButtonClick}
                        disabled={isConfirmCodeGotten}>{isConfirmCodeGotten ? count + '秒' : '获取验证码'}
                    </Button>
                </Box>
                {/* 新密码 */}
                <Collapse in={isConfirmCodeGotten} >
                    <Box sx={{ pb: '8px' }}>
                        <Password
                            text="密码"
                            id="user_password"
                            showPassword={showPassword}
                            placeholder="密码"
                            defaultValue={state.data.password}
                            onClick={() => handlePasswordShowClick(() => setShowPassword((show) => (!show)), 'user_password')}
                            onChange={(tar) => setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    password: tar.target.value
                                },
                                isPasswordValid: true,
                            })}
                            onBlur={(tar) => setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    password: tar.target.value
                                },
                                isPasswordValid: !tar.target.value.length || valid(tar.target.value, PasswordPattern, 'g'),
                                isConfirmPasswordValid: state.data.confirmPassword == "" ? true : (!tar.target.value.length || (valid(state.data.confirmPassword, PasswordPattern, 'g') && state.data.password == state.data.confirmPassword))
                            })}
                            onKeyUp={handleKey}
                            error={!state.isPasswordValid}
                        />
                    </Box>
                    {/* 确认密码 */}
                    <Box sx={{ pb: '16px' }}>
                        <Password
                            text="密码确认"
                            id="confirm_password"
                            showPassword={showConfirmPassword}
                            placeholder="确认密码"
                            defaultValue={state.data.confirmPassword}
                            onClick={() => handlePasswordShowClick(() => setShowConfirmPassword((show) => (!show)), 'confirm_password')}
                            onChange={(tar) => setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    confirmPassword: tar.target.value
                                },
                                isConfirmPasswordValid: true,
                            })}
                            onBlur={(tar) => setState({
                                ...state,
                                data: {
                                    ...state.data,
                                    confirmPassword: tar.target.value
                                },
                                isConfirmPasswordValid: (!tar.target.value.length || (valid(tar.target.value, PasswordPattern, 'g') && state.data.password == state.data.confirmPassword))
                            })}
                            onKeyUp={handleKey}
                            error={!state.isConfirmPasswordValid}
                        />
                    </Box>
                    <Divider />
                    <Box sx={{ mt: '16px' }}>
                        <Submit onClick={ submitButtonClick } />
                    </Box>
                </Collapse>
            </Box>
        </div>
    );
}