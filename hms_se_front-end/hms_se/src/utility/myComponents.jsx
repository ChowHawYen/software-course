﻿import React, { useEffect, useRef, useState } from 'react';
import { Visibility, VisibilityOff, Send, UploadFile, CloudOutlined, CloudUploadOutlined, CloudDoneOutlined, CloudOffOutlined, KeyboardArrowRight, KeyboardDoubleArrowRight, KeyboardArrowLeft, Favorite, FavoriteBorder } from '@mui/icons-material';
import { FormControl, OutlinedInput, InputLabel, IconButton, Paper, Button, Grid, List, Checkbox, LinearProgress, Box, Backdrop, CircularProgress, Tooltip, MobileStepper, Rating } from '@mui/material'
import { ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import { styled } from '@mui/material/styles';
import { Tab } from '@mui/material'
import { TabPanel, TabContext } from '@mui/lab';
import { useTheme } from '@mui/material/styles';
import { ImagesList } from './constants.jsx';
import { not, intersection } from './tool.jsx';

export function Password(props) {
    return (
        <FormControl id="password_input" sx={{ m: 1, width: '48ch', bgcolor: 'white' }} variant="outlined">
            <InputLabel htmlFor="user_password" >{props.text}</InputLabel>
            <OutlinedInput
                id={props.id}
                type={props.showPassword ? 'text' : 'password'}
                color="primary"
                endAdornment={
                    <IconButton
                        aria-label="toggle password visibility"
                        onClick={props.onClick}
                        onMouseDown={(event) => { event.preventDefault() }}
                        edge="end"
                    >
                        {/* 显示密码 */}
                        {props.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                }
                defaultValue={props.defaultValue}
                onChange={props.onChange}
                onBlur={props.onBlur}
                onKeyUp={props.onKeyUp}
                error={props.error}
                label={props.text}
                placeholder={props.placeholder}
            />
        </FormControl>
    )
}
export function UserOperation(props) {
    return (
        <ListItem key={props.id} disablePadding>
            <ListItemButton onClick={props.onClick}>
                <ListItemIcon>
                    {props.icon}
                </ListItemIcon>
                <ListItemText primary={props.text} />
            </ListItemButton>
        </ListItem>)
}

export function HiddenLine(props) {
    return (
        <Box sx={{ height: props.height, width: '100%' }}></Box>
    )
}

export function Submit(props) {
    return (
        <Button
            variant="contained"
            endIcon={<Send />}
            onClick={props.onClick}
        >提　交</Button>
    )
}

export function TransferList(props) {
    const [checked, setChecked] = React.useState([]);

    const leftChecked = intersection(checked, props.fileList);

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const processIcon = (s) => {
        switch (s) {
            case 'waiting': return <CloudOutlined color='primary'/>;
            case 'process': return <CloudUploadOutlined color='primary' />;
            case 'success': return <CloudDoneOutlined color='success' />;
            case 'failure': return <CloudOffOutlined color='error' />
        }
    }

    const retry = (e) => {
        let value = e.target.innerHTML

        if (props.status.get(value) == 'failure') {
            props.retry([...props.fileList, value], not(props.uploadList, value), value)
        }
    }

    const handleAllRight = () => {
        props.uploadFile([], [...props.uploadList, ...props.fileList]);
    };

    const handleCheckedRight = () => {
        props.uploadFile(not(props.fileList, leftChecked), [...props.uploadList, ...leftChecked])
    };

    const customList = (items, upload) => (
        <Paper sx={{ width: 400, height: 500 }} elevation={16} >
            <List dense component="div" role="list">
                {items.map((value) => {
                    const labelId = `transfer-list-item-${value}-label`;
                    return (
                        <Box key={'file-' + value}>
                            <ListItem
                                role="listitem"
                                button
                                onClick={handleToggle(value)}
                            >
                                <ListItemIcon>
                                    {!upload ?
                                        <Checkbox
                                            checked={checked.indexOf(value) !== -1}
                                            tabIndex={-1}
                                            disableRipple
                                            inputProps={{
                                                'aria-labelledby': labelId,
                                            }}
                                        /> : processIcon(props.status.get(value))
                                    }
                                </ListItemIcon>
                                <ListItemText sx={{ mt: '12px' }} id={labelId} primary={value} onClick={retry} />
                            </ListItem>
                            <ListItem>
                                {upload ?
                                    <Box sx={{ display: 'block',width: '100%' }}>
                                        <LinearProgress
                                            color={props.status.get(value) == 'failure' ? 'error' : (props.status.get(value) == 'success' ? 'success' : 'primary')}
                                            value={props.status.get(value) == 'waiting' ? 0 : 100}
                                            variant={props.status.get(value) == 'processing' ? 'indeterminate' : 'determinate'} />
                                    </Box>  : void(0)
                                    }
                            </ListItem>
                        </Box>
                    );
                })}
            </List>
        </Paper>
    );

    return (
        <Grid container spacing={2} justifyContent="center" alignItems="center">
            <Grid item>{customList(props.fileList, false)}</Grid>
            <Grid item>
                <Grid container direction="column" alignItems="center" justifyContent="space-between" sx={{height: '250px'}}>
                    <CustomerizedIconButton
                        title="全部上传"
                        onClick={handleAllRight}
                        disabled={props.fileList.length === 0}
                        icon={<KeyboardDoubleArrowRight />}
                    />
                    <CustomerizedIconButton     
                        title="上传所选文件"
                        onClick={handleCheckedRight}
                        disabled={leftChecked.length === 0}
                        icon={<KeyboardArrowRight />}
                    />
                    <CustomerizedIconButton
                        title="选择文件"
                        onClick={(e) => e.target.value = null}
                        icon={<UploadFile />}
                        hidden={<input type="file" hidden='hidden' onChange={props.selectFile} accept='json/*' />}
                    />
                </Grid>
            </Grid>
            <Grid item>{customList(props.uploadList, true)}</Grid>
        </Grid>
    );
}

export function Loading() {
    return (
        <Backdrop open={true}>
            <CircularProgress color="inherit" />
        </Backdrop>
    )
}

export function CustomerizedIconButton(props) {
    return (
        <Tooltip title={props.title} placement="bottom-end">
            <span>
                <IconButton
                    sx={{ my: 0.5 }}
                    variant="outlined"
                    component="label"
                    edge={props.edge}
                    size={props.size ? props.size : 'small'}
                    onClick={props.onClick}
                    disabled={props.disabled}
                    aria-label="move all right"
                    color={props.color ? props.color : "primary"}
                >
                    {props.icon}
                    {props.hidden}
                </IconButton>
            </span>
        </Tooltip>
    )
}

export function CustomerizedSwipeableViews(props) {

    const theme = useTheme();
    const [activeStep, setActiveStep] = useState(0);
    const [count, setCount] = useState(0);
    const timerRef = useRef(null);
    const maxSteps = props.size;

    timerRef.current = () => {
        setCount(c => c + 1);
    }

    useEffect(() => {
        let id = setInterval(() => {
            timerRef.current();
        }, 1000);
        return () => clearInterval(id);
    }, []);

    const handleNext = () => {
        setActiveStep((prevActiveStep) => (prevActiveStep + 1) % maxSteps);
        setCount(0)
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => (prevActiveStep - 1 + maxSteps) % maxSteps);
        setCount(0)
    };

    if (count == 5) {
        handleNext()
    }

    return (
        <Box sx={{ flexGrow: 1, justifyContent:"center", alignItems:"center" }}>
            <Paper
                square
                elevation={0}
                sx={{
                    display: 'flex',
                    justifyContent: "center",
                    alignItems: 'center',
                    height: 325,
                    pl: 2,
                    bgcolor: 'background.default',
                }}
            >
                <TabContext value={String(activeStep)}>
                    {
                        ImagesList.map((image, index) => {
                            return (
                                <TabPanel value={String(index)} key={index}>
                                    <Box
                                        component="img"
                                        src={image.path}
                                        sx={{
                                            display: 'flex',
                                            justifyContent: "center",
                                            alignItems: 'center',
                                            display: 'block',
                                            overflow: 'hidden',
                                            objectFit: 'contain',
                                            width: '100%',
                                            height: 300
                                        }}
                                    >
                                    </Box>
                                </TabPanel>
                            )
                        })
                    }
                </TabContext>
                
            </Paper>
            <MobileStepper
                steps={maxSteps}
                position="static"
                activeStep={activeStep}
                nextButton={
                    <Button
                        size="small"
                        onClick={handleNext}
                    >
                        {theme.direction === 'rtl' ? (
                            <KeyboardArrowLeft />
                        ) : (
                            <KeyboardArrowRight />
                        )}
                    </Button>
                }
                backButton={
                    <Button size="small" onClick={handleBack}>
                        {theme.direction === 'rtl' ? (
                            <KeyboardArrowRight />
                        ) : (
                            <KeyboardArrowLeft />
                        )}
                    </Button>
                }
            />
        </Box>
    );
}

export let CustomerizedRating = (props) => {
    const StyledRating = styled(Rating)({
        '& .MuiRating-iconFilled': {
            color: '#ff6d75',
        },
        '& .MuiRating-iconHover': {
            color: '#ff3d47',
        },
    });

    return (
        <Box sx={{ position: 'relative' }}>
            <Tooltip title={props.value * 20} placement="bottom-end">
                <span>
                    <StyledRating
                        name="customized-color"
                        value={props.value}
                        precision={0.1}
                        icon={<Favorite fontSize="inherit" />}
                        emptyIcon={<FavoriteBorder fontSize="inherit" />}
                        readOnly
                    />
                </span>
            </Tooltip>
        </Box>
    )
}