﻿import React from 'react';
import { Analytics, PermIdentity, FileUpload } from '@mui/icons-material';

//--------------------------------------------------------------------------------
// regular expression pattern
//--------------------------------------------------------------------------------
export let EmailPattern = "^([a-z0-9]+[-._])*[a-z0-9]+@[a-z0-9][a-z0-9\-]*([\.][a-z]{2,}){1,62}$"
export let PhoneNumberPattern = "^[0-9]{11}$"
export let PasswordPattern = "^[-_.!@#$%^&*0-9a-zA-Z]{6,}$"
export let NumberPattern = "^[0-9]{2,3}(.[0-9])?$"

//--------------------------------------------------------------------------------
//http option
//--------------------------------------------------------------------------------
export let DefaultURL = "http://127.0.0.1:5000"
export let LoginURL = "/api/login"
export let RegisterURL = "/api/register"
export let SearchURL = "/api/search"
export let ModifyURL = "/api/modify"
export let UploadURL = "/api/upload"
export let ReportURL = "/api/predict"
export let RetrieveURL = "/api/retrieve"
export let RetrieveVarifyURL = "/api/retrieve/varify"
export let option = {
    headers: { 'Content-Type': 'application/json' },
}
export let fileOption = {
    headers: {
        'Content-Type': 'multipart/form-data'
    }
}
//--------------------------------------------------------------------------------
//message
//--------------------------------------------------------------------------------
export let RegisterSuccess = '注册成功。'
export let ConfirmCodeSent = '已向{0}发送验证码。'
export let ModifySuccess = '{0}修改成功。'
export let InCompleteInput = '请输入{0}。'
export let UncorretPassword = '请输入6位以上且仅包含大小写字母，数字，特殊符号-_.!@#$%^&*的密码。'
export let NonNumeric = '{0}请输入数字。'
export let UncorrectPhoneNumber = '请输入正确格式的电话号码。'
export let UncorrectEmail = '请输入正确格式的邮箱地址。'
export let UnmatchedPassword = '密码不一致，请重新确认。'
//--------------------------------------------------------------------------------
//enum
//--------------------------------------------------------------------------------
export let GenderList = [
    {
        value: 9,
        label: '　'
    },
    {
        value: 0,
        label: '男'
    },
    {
        value: 1,
        label: '女'
    },
    {
        value: 2,
        label: '保密'
    }
]    

export let Operations = [
    {
        key: 'report',
        text: '我的报告',
        icon: <Analytics />,
    },
    {
        key: 'upload',
        text: '上传文件',
        icon: <FileUpload />,
    },
    {
        key: 'info',
        text: '个人信息',
        icon: <PermIdentity />,
    },
]

export let AlterList = [
    {
        value: true,
        label: '是'
    },
    {
        value: false,
        label: '否'
    }
]

export let ImagesList = [
    {
        label: 'healthy China',
        path: '../../public/healthy_china.webp'
    },
    {
        label: 'healthy tech',
        path: '../../public/healthy_tech.webp'
    },
    {
        label: 'smart watch',
        path: '../../public/smart_watch.png'
    },
    {
        label: 'percentage',
        path: '../../public/percentage.png'
    },
    {
        label: 'consume',
        path: '../../public/consume.png'
    }
]

export let UserManual = [
    {
        title: 'Introduction',
        details: [
            {
                subtitle: 'preface',
                text: [
                    `　Welcome to the User Manual for the V1 Huawei Smart Band Health Data Portal. This software is designed to seamlessly integrate with your Huawei smart band, enabling you to conveniently upload and analyze your health data on our website. With this innovative tool, you gain access to a comprehensive health report generated from the information collected by your smart band.`,
                    `　The Huawei Smart Band Health Data Portal serves as a centralized platform, allowing you to harness the power of your health-related metrics and transform them into valuable insights. Whether you're monitoring your daily activity, sleep patterns, heart rate, or other vital health indicators captured by your Huawei smart band, this software empowers you to delve deeper into your well-being.`,
                ]
            },
            {
                subtitle: 'Key features',
                text: [
                    `　Data Upload: Easily transfer health data gathered by your Huawei smart band onto our secure website.`,
                    `　Comprehensive Analysis: Obtain a detailed health report that interprets your collected data, providing valuable insights into your health status and trends.`,
                    `　Personalized Recommendations: Receive personalized suggestions and recommendations based on the analyzed health metrics to help you make informed decisions regarding your lifestyle and fitness goals.`,
                    `　User-Friendly Interface: Navigate a user-friendly interface designed to streamline the process of uploading data and accessing your health reports.`,
                ]
            },
            {
                subtitle: 'Getting Started',
                text: [
                    `　This user manual is crafted to guide you through the seamless integration of your Huawei smart band with our V1 software, providing step-by-step instructions on data uploading, report retrieval, and utilizing the various features available.`,
                    `　We encourage you to explore the functionalities of the Huawei Smart Band Health Data Portal to maximize the benefits of tracking and understanding your health metrics.`,
                    `　Should you encounter any queries or require assistance during your journey with our V1 software, our dedicated support team is readily available to assist you.`,
                    `　Thank you for choosing our platform to enhance your health tracking experience. Let's embark on this journey towards better health together!`,
                ]
            }
        ]
        
    },
    {
        title: 'Operating Instructions',
        details: [
            {
                subtitle: 'Register',
                text: [
                    `1.Open your web browser and go to the website.`,
                    `2.Access the registration page and click on “register” to proceed to the registration.`,
                    `3.On the registration page, you will need to enter your personal information such as :`,
                    `　　a.Full name`,
                    `　　b.Username`,
                    `　　c.Email address`,
                    `　　d.Password (Ensure it meets the requirements)`,
                    `　　e.Additional information (e.g., date of birth)`,
                    `4.Once completed, click on “submit” to finalize the process.`,
                    `　`,
                    `Additional Tips:`,
                    `　●Make sure to use a strong and unique password to enhance the security of your account.`,
                    `　●Keep your login credentials (username and password) in a secure place and do not share them with anyone.`,
                ]
            },
            {
                subtitle: 'Login',
                text: [
                    `1.Open your web browser and go to the website.`,
                    `2.Access the login page and click on “login” to proceed to the login.`,
                    `3.On the login page, enter the credentials associated with your account:`,
                    `　　a.Username`,
                    `　　b.Password`,
                    `4.After entering your credentials, click on the "Log In" button.`,
                    `　`,
                    `Additional Tips:`,
                    `　●Remember to log out after you've finished using the service, especially when using a shared or public computer.`,
                    `　●Avoid using "Remember Me" or "Stay Logged In" options on public devices for security reasons.`,
                    `　●If you forget your password, most services have a "Forgot Password" option to reset it.`,
                ]
            },
            {
                subtitle: 'Forgot Password',
                text: [
                    `1.Open your web browser and go to the website.`,
                    `2.Access the login page and click on “Forgot Password”.`,
                    `3.On the password reset page, you'll be prompted to enter information to identify your account such as :`,
                    `　　a.Username`,
                    `4.Enter the required details accurately.`,
                    `5.A code will be sent to your email address, you will need to enter the code into the website.`,
                    `6.Once your account is identified and verified, you'll be prompted to create a new password.`,
                    `7.Enter a new password following the specified requirements.`,
                    `8. After entering and confirming the new password, submit the changes by clicking on the "Reset Password" button.`,
                    `　`,
                    `Additional Tips:`,
                    `　●Choose a strong and unique password to enhance the security of your account.`,
                    `　●Keep your login credentials (username and password) in a secure place and do not share them with anyone.`,
                ]
            },
            {
                subtitle: 'Get health data',
                text: [
                    `1.Open your web browser and go to Huawei website.`,
                    `2.Access the Huawei login page and log in.`,
                    `3.Access your health information.`,
                    `4.Export your data.`,
                    `　`,
                    `Additional Tips:`,
                    `　●Getting your data can take up to 1 week.`,
                ]
            },
            {
                subtitle: 'Upload health information',
                text: [
                    `1.Open your web browser and log in.`,
                    `2.Access the upload page.`,
                    `3.Get your health data and double click on it.`,
                    `4.Open the “Health detail data & description” folder.`,
                    `5.Take the “Health detail data.json” file, take it and drop it on the upload page.`,
                    `6.If you have multiple data file, you can put multiple files on the website.`,
                    `7.Choose the specific file that you want to upload.`,
                    `8.Click on “upload” button.`,
                    `　`,
                    `Additional Tips:`,
                    `　●Be careful to take the right file.`,
                    `　●In the V1, the software can only support Huawei smart band data structures.`,
                ]
            },
            {
                subtitle: 'Get health report',
                text: [
                    `1.Open your web browser and go to the website.`,
                    `2.Access the “report” page.`,
                    `3.Select a time range for your report.`,
                    `4.Click on “Generate” to get your report.`,
                ]
            },
        ]
    },
    {
        title: 'Troubleshooting',
        details: [
            {
                subtitle: 'Troubles',
                text: [
                    `1.If you encounter lost connections, check your connection and refresh your page.`,
                    `2.If you cannot upload your data, maybe the software is not able to take care of your data structure right now, you will need to wait for a newer version.`,
                    `3.If you have a error in the generation of a health report, maybe you don’t have data in the chosen time plage.`,
                    `4.If you have an error in the username in registration, maybe the username is already used.`,
                    `5.If health report are not generated correctly, check if the health data uploaded from the smart band is accurate and complete. Verify that there are no missing or incorrect data points in the Huawei Health app.`,
                    `6.If you forgot your password for website, you can follow the “Forgot Password” instructions.`,
                ]
            }
        ]
    },
    {
        title: 'Safety information',
        details: [
            {
                subtitle: 'Data privacy and security',
                text: [
                    `1.We do not share your health data with unauthorized third parties. Your information is used strictly for generating your personalized health report.`,
                    `2.Measures against SQL Injection.`,
                ]
            },
            {
                subtitle: 'User responsibility',
                text: [
                    `1.Protect your login credentials. Keep your username and password confidential. Avoid sharing this information with others to prevent unauthorized access to your account.`,
                    `2.Encoding your password before adding to the database.`,
                ]
            },
            {
                subtitle: 'Device safety',
                text: [
                    `1.Ensure your Huawei smart band is properly configured and updated with the latest firmware to mitigate potential security vulnerabilities.`,
                ]
            }
        ]
    },
    {
        title: 'Contact Information',
        details: [
            {
                subtitle: 'Team Leader',
                text: [
                    `Contact：杨文泰`,
                    `Phone：18717787041`,
                    `Email：476027961@sjtu.edu.cn`,
                ]
            },
        ]
    }
    
    
]
