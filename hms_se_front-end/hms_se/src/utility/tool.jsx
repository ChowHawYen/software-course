//--------------------------------------------------------------------------------
// validator
//--------------------------------------------------------------------------------
export let valid = (value, exp, mode, allowEmpty=false) => {
    if (allowEmpty && (value == undefined || value.length == 0)) return true;
    var reg = new RegExp(exp, mode)
    return reg.test(value)
}

//--------------------------------------------------------------------------------
// tool
//--------------------------------------------------------------------------------
export let format = (str, ...args) => {
    args.forEach((e) => str = str.replace(/[\{][0-9]?[\}]/i, e))
    return str
}

export let not = (a, b) => {
    return a.filter((value) => b.indexOf(value) === -1);
}

export let intersection = (a, b) => {
    return a.filter((value) => b.indexOf(value) !== -1);
}

export let min = (a, b) => {
    return (a.isAfter(b) ? b : a)
}

export let max = (a, b) => {
    return (a.isAfter(b) ? a : b)
}

export let isSameHour = (date1, date2) => {
    return (
        date1.getFullYear() == date2.getFullYear() && // 年份相等
        date1.getMonth() == date2.getMonth() && // 月份相等
        date1.getDay() == date2.getDay() && // 星期相等
        date1.getHours() == date2.getHours() // 小时数相等
    );
}