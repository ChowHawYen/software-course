import React, { useState, Suspense } from 'react';
import Home from './pages/home.jsx';
import Login from './pages/login.jsx';
import Navigation from './pages/navigation.jsx';
import Register from './pages/register.jsx';
import Report from './pages/report.jsx';
import Upload from './pages/upload.jsx';
import Info from './pages/info.jsx';
import Retrieve from './pages/retrieve.jsx'
import { createBrowserRouter, createRoutesFromElements, RouterProvider, Route, Outlet, Navigate } from 'react-router-dom';

export default function App() {
    const [state, setState] = useState({
        login: false,
        username: "",
        name: "",
        message: "",
        messageShow: false,
        messageType: "success"
    })

    const setLoginStatus = (login, username, name) => {
        setState({
            ...state,
            login: login,
            username: username,
            name: name
        })
    }

    const setMessage = (msg, show, type) => {
        setState({
            ...state,
            message: msg,
            messageShow: show,
            messageType: type
        })
    }

    const router = createBrowserRouter(
        createRoutesFromElements(
            <Route element={<div>
                <Navigation state={state} logout={() => setLoginStatus(false, "", "")} setMessage={(m, s, t) => setMessage(m, s, t)} />
                <Outlet />
            </div>}>
                <Route exact path="/" element={<Navigate to='/home'/>} />
                <Route exact path="/home" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Home state={state} />
                    </Suspense>} />
                <Route exact path="/login" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Login
                            state={state}v
                            login={(u, n) => setLoginStatus(true, u, n)}
                            setMessage={(m, s, t) => setMessage(m, s, t)}
                        />
                    </Suspense>} />
                <Route exact path="/register" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Register
                            state={state}
                            setMessage={(m, s, t) => setMessage(m, s, t)} />
                    </Suspense>} />
                <Route exact path="/report" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Report
                            state={state}
                            setMessage={(m, s, t) => setMessage(m, s, t)} />
                    </Suspense>} />
                <Route exact path="/upload" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Upload
                            state={state}
                            setMessage={(m, s, t) => setMessage(m, s, t)} />
                    </Suspense>} />
                <Route exact path="/info" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Info
                            state={state}
                            setMessage={(m, s, t) => setMessage(m, s, t)} />
                    </Suspense>} />
                <Route exact path="/retrieve" element={
                    <Suspense fallback={<div>loading</div>}>
                        <Retrieve
                            state={state}
                            setMessage={(m, s, t) => setMessage(m, s, t)} />
                    </Suspense>} />
            </Route>
        )
    );

    return (
        <React.StrictMode>
            <RouterProvider router={router}>
            </RouterProvider>
        </React.StrictMode>
    )
} 

