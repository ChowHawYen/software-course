﻿import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App.jsx'
import $ from 'jquery';
import './css/index.css'
import './css/main.css'

const container = $('#root')[0]
const root = createRoot(container)
root.render(<App />)