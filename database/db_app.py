from flask import Flask, request, jsonify
import os
import sqlite3
import yaml
import logging
import re

# SETUP LOGGING SYSTEM
logging.basicConfig(level=logging.DEBUG,  
                    format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

error_file_handler = logging.FileHandler('db_error.log')
error_file_handler.setLevel(logging.ERROR)  # Set the level for this handler
info_file_handler = logging.FileHandler('db_info.log') 
info_file_handler.setLevel(logging.INFO)  # Set the level for this handler

file_format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
error_file_handler.setFormatter(file_format)
info_file_handler.setFormatter(file_format)

logger.addHandler(info_file_handler)
logger.addHandler(error_file_handler)

""" Database class, defined related operation """
class Database:
    """
    Database Object that should be created only once.
    function always return with status code and status detail
    >>> db = Database(config_path)
    >>> db.query_db("SELECT * FROM user") # execute any query you want
    """
    def __init__(self, config:dict) -> None:
        try:
            """ data initialize """
            self.config = config # save configuration
            self.connection = None # save db connection
            """ Generate connection """
            self.connection = sqlite3.connect(self.config['default_database'],check_same_thread=False)
        except Exception as err:
            print(err)
    
    def __str__(self) -> str: 
        return 'database: {}, connected: {}'.format(self.config['default_database'],  self.connection is not None)
    
    def update_db(self, drop_table = False) -> dict:
        """
        expand or contract the db using new template,
        do not clear data unless the column no longer exists
        input: 
        drop_table - drop table/column when old table/column not exist in new template \\
        output: <dict> of new database structure
        >>> print(db.update_db())
        {"statue_code": "OK", "Info": {"Table1": [list_of_column], "Table2": [list_of_column]}}
        """
        """ Initialization """
        """ TODO Waiting for implementation """
        current_tables_dict = {}
        new_table_dict = {}
        status_code = 'OK'
        try:
            """ Read db info """
            current_tables_dict = self.return_database_status()['table'] # read old database info
            for key, value in self.config["db_template"].items(): pass # read new config info
            """ Compare db info """
            table_added = [key for key in new_table_dict if key not in current_tables_dict]
            table_removed = [key for key in current_tables_dict if key not in new_table_dict]
            table_modified = [key for key in new_table_dict if key in current_tables_dict and new_table_dict[key] != current_tables_dict[key]]
            """ Execute update """
        except Exception as err:
            status_code = err
            logger.error("An error occurred: %s", err, exc_info=True)
        return {
            "status_code": status_code, 
            }

    def query_db(self, qry:dict) -> dict:
        """
        input: qry<dict> The query dict directly from flask 
        {"query": <str>, "param_input": <dict>} 
        output: <dict>
        >>> data = query_db("select * from username;")
        >>> data
        {"statue_code": "OK", "data": {queried_data}}
        """
        """ Initialization """
        status_code = 'OK'
        data = None
        try:
            # use parameterized query to prevent injection
            """ Preprocess incoming query """
            sql_query = qry['query']
            sql_param = qry['param_input']
            placeholders = [p.strip('{}') for p in re.findall(r'{.*?}', sql_query)]
            sql_param = {placeholder: sql_param[placeholder] for placeholder in placeholders} # reorder the param in the dict
            
            if "=" in sql_param.values(): raise

            sql_query = re.sub(r'{[^{}]+}', '?', sql_query) # replace the query input
            """ Create connection and execute query """
            curr = self.connection.cursor()
            curr.execute(sql_query, tuple(sql_param.values()))
            self.connection.commit()
            """ Read query result """
            data = curr.fetchall()
        except Exception as err:
            status_code = err
            logger.error("An error occurred: %s", err, exc_info=True)
        return {
            "status_code": status_code, 
            "data": data
            }
    
    def query_insert_mul(self, qry:dict) -> dict:
        """ Initialization """
        status_code = 'OK'
        data = None
        try:
            # use parameterized query to prevent injection
            """ Preprocess incoming query """
            sql_query = qry['query']
            sql_param = qry['data_list']
            placeholders = [p.strip('{}') for p in re.findall(r'{.*?}', sql_query)]
            sql_query = re.sub(r'{[^{}]+}', '?', sql_query) # replace the query input
            
            """ Create connection and execute query """
            curr = self.connection.cursor()
            curr.executemany(sql_query, sql_param)

            self.connection.commit()
            """ Read query result """
            data = curr.fetchall()
        except Exception as err:
            status_code = err
            logger.error("An error occurred: %s", err, exc_info=True)
        return {
            "status_code": status_code, 
            "data": data
            }

    def recreate_db(self) -> dict:
        """
        wipe out current database and recreate it using template
        input: None
        output: <dict>
        >>> print(db.recreate_db())
        >>> {"status_code": "OK"}
        """
        """ Initialization """
        status_code = 'OK'
        create_command_list = []
        try:
            """ Close current connection """
            """Wipe out the old db and create a new one"""
        
            """ Create command list """
            # TODO Not a good way to implement
            for tables, columns in self.config["db_template"].items(): # per item is a table
                column_str = ''
                for column in columns: # per item is one column (name, type)
                    column_str += ', {}'.format(column)
                create_command = 'CREATE TABLE {}({});'.format(tables, column_str[2:]) # prepare a new create command
                create_command_list.append(create_command)
            """ Open database connection and apply it """
            temp_conn = sqlite3.connect(self.config['default_database'])
            curr = temp_conn.cursor()
            for command in create_command_list:
                curr.execute(command)
                temp_conn.commit()
            temp_conn.close()
            """ Regenerate connection """
            self.connection = sqlite3.connect(self.config['default_database'])
        except Exception as err:
            status_code = err
            logger.error("An error occurred: %s", err, exc_info=True)
        return {
            "status_code": status_code, 
            }

    def return_database_status(self):
        """
        return database basic status,mainly configuration  
        input: None
        output: <dict> status code
        >>> print(db.return_database_status())
        {"status_code" = 'OK', "database":"main.db", "connected":true, "table" = {}}
        """
        """ Initialization """
        status_code = 'OK'
        current_tables_dict = {}
        try:
            """ Query info from current connection """
            curr = self.connection.cursor() 
            curr.execute("SELECT name FROM sqlite_master WHERE type='table';")
            tables = curr.fetchall() # read all current table info
            for table in tables:
                table_name = table[0]
                curr.execute(f"PRAGMA table_info({table_name});")
                columns = curr.fetchall()
                column_list = []
                for column in columns:
                    column_name = column[1]
                    column_list.append(column_name)
                current_tables_dict[table_name] = column_list
        except Exception as err:
            status_code = err
            logger.error("An error occurred: %s", err, exc_info=True)
        return {
            "status_code": status_code, 
            "database": self.config['default_database'],
            "connected": self.connection is not None,
            "table": current_tables_dict
            }

""" Main app """
config_path = 'config.yaml'
with open(config_path, "r") as file: config = yaml.safe_load(file)
app = Flask(__name__)
db_connection = Database(config) # Create an object

""" Report database info"""
""" GET """
""" <ip>:5050/"""
@app.route('/')
def database_report():
    data = {}
    re_code = 200
    try:
        data = db_connection.return_database_status()
    except Exception as err:
        data["Error"] = str(err)
        re_code = 500 # internal server error
        logger.error("An error occurred: %s", err, exc_info=True)
    return data, re_code

""" qry database database info"""
""" POST """
""" <ip>:5050/qry"""
@app.route('/qry', methods=['POST'])
def qry():
    """
    input: 
    { "query": <str>, "param_input": <dict>}  
    """
    data = {}
    try:
        incoming_qry = request.get_json()
        data = db_connection.query_db(incoming_qry)
    except Exception as err:
        data["Error"] = err
        #re_code = 500 # internal server error
        logger.error("An error occurred: %s", err, exc_info=True)
    return jsonify(data)

@app.route('/ins_mul', methods=['POST'])
def ins_mul():
    data = {}
    try:
        incoming_qry = request.get_json()
        data = db_connection.query_insert_mul(incoming_qry)
    except Exception as err:
        data["Error"] = err
        #re_code = 500 # internal server error
        logger.error("An error occurred: %s", err, exc_info=True)
    return jsonify(data)


""" recreate the db"""
""" POST """
""" <ip>:5050/recdb"""
@app.route('/recdb', methods=['POST'])
def rec():
    data = {}
    re_code = 200
    try:
        data = db_connection.recreate_db()
    except Exception as err:
        data["Error"] = str(err)
        re_code = 500 # internal server error
        logger.error("An error occurred: %s", err, exc_info=True)
    return data, re_code

if __name__ == '__main__':
    app.run(host=config['dbhost'], port=config['dbport'])
