import yaml
from db_app import Database
with open('config.yaml', "r") as file: config = yaml.safe_load(file)

db_connection = Database(config)
db_connection.recreate_db()