from .conftest import *
import threading

def make_api_requests():
    data_send = {
            "query": TEST_INSERT_QRY1,
            "param_input": TEST_INSERT_QRY_PARAM1
            }
    qry_man(data_send)

def test_sequential_insert():
    seq_num = 20
    #### Insert into user many times ####
    data_send = {
            "query": TEST_INSERT_QRY1,
            "param_input": TEST_INSERT_QRY_PARAM1
            } 
    for i in range(seq_num):
        data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK' # check the insertion success

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert len(data_rev['data']) == 20
    
    #### Delete the user ####
    data_send = {
            "query": TEST_DELETE_QRY1,
            "param_input": TEST_DELETE_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK' # check the insertion success

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert data_rev['data'] == []

def test_parallel_insert():
    num_threads = 20
    threads = []

    for i in range(num_threads):
        thread = threading.Thread(target=make_api_requests)
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert len(data_rev['data']) == 20

    #### Delete the user ####
    data_send = {
            "query": TEST_DELETE_QRY1,
            "param_input": TEST_DELETE_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK' # check the insertion success

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert data_rev['data'] == []
    
    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert data_rev['data'] == []
    
    