import subprocess
import yaml
import sys, os
from time import sleep
import requests as req
import pytest

def qry_man(data_send:dict):
    res = req.post(db_server_qry_url, json=data_send)
    return res.json()

# test qry insert 1
TEST_INSERT_QRY1 = "INSERT INTO user (name, birthday, height, weight, h_info, gender, been_drinking, been_smoking, username, password, is_premium)\
VALUES ({name}, {birthday}, {height}, {weight}, {h_info}, {gender}, {been_drinking}, {been_smoking}, {username}, {password}, {is_premium})"
TEST_INSERT_QRY_PARAM1 = {
                "name": "Dominique",
                "birthday": '2023-11-12',
                "height": 178,
                "weight": 68,
                "h_info": "Healthy",
                "gender": "Male",
                "been_drinking": True,
                "been_smoking": False,
                "username": "Huang",
                "password": "password",
                "is_premium": True
                }

# test qry select 1
TEST_SELECT_QRY1 = "SELECT * FROM user WHERE name = {name}"
TEST_SELECT_QRY_PARAM1 = {
                "name": "Dominique"
                }

TEST_UPDATE_QRY1 = "UPDATE user SET password = {password} WHERE name = {name}"
TEST_UPDATE_QRY_PARAM1 = {
                "password": "abetterpassword",
                "name":"Dominique"
                }

# test qry delete 1
TEST_DELETE_QRY1 = "DELETE FROM user WHERE name = {name}"
TEST_DELETE_QRY_PARAM1 = {
                "name": "Dominique"
                }

# test sql injection
TEST_INJECTION_QRY1 = "SELECT * FROM user WHERE name = 105 OR 1=1"
TEST_INJECTION_QRY_PARAM1 = {}


original_working_dir = os.getcwd()
test_script_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(test_script_dir)
launch_file_path = './launch start'
stop_file_path = './launch stop'
os.chdir(original_working_dir)

db_server_url = "http://127.0.0.1:5050/"
db_server_qry_url = db_server_url + 'qry'

def pytest_sessionstart(session): # guys this is the function executed when the test started
    print("--Database unittest session has started--")

def pytest_sessionfinish(session): # and this is when it is ended
    print("\n--Database unittest session has stopped--")
