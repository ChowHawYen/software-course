from .conftest import *
    
def test_getbasicinfo():
    """ Try to get basic information of the db """
    res = req.get(db_server_url)
    assert (res is not None and res.text != '{}') # assert the db can respond with content
    assert ('connected' in res.text) # check the content is valid

def test_insertnewdata():
    """ Try to insert new row of data """

    #### Insert into user ####
    data_send = {
            "query": TEST_INSERT_QRY1,
            "param_input": TEST_INSERT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK' # check the insertion success

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert "Dominique" in data_rev['data'][0]

def test_updatedata():
    """ Try to update existed data """
    #### Update user ####
    data_send = {
            "query": TEST_UPDATE_QRY1,
            "param_input": TEST_UPDATE_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK' # check the insertion success

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert "abetterpassword" in data_rev['data'][0]

def test_dataconsistancy():
    """ Does the data really saved into database """
    """ Reconnect the database """
    subprocess.run([stop_file_path], cwd=test_script_dir,stdout=subprocess.PIPE, shell=True)
    sleep(1)
    subprocess.run([launch_file_path], cwd=test_script_dir, stdout=subprocess.PIPE,shell=True)
    sleep(2)

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert "Dominique" in data_rev['data'][0]
    assert "abetterpassword" in data_rev['data'][0]
    
def test_deletedata():
    """ Try to delete existed data """

    #### Delete the user ####
    data_send = {
            "query": TEST_DELETE_QRY1,
            "param_input": TEST_DELETE_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK' # check the insertion success

    #### Select the user ####
    data_send = {
            "query": TEST_SELECT_QRY1,
            "param_input": TEST_SELECT_QRY_PARAM1
            } 
    data_rev = qry_man(data_send)
    assert data_rev['status_code'] == 'OK'
    assert data_rev['data'] == []
    
