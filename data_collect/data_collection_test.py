import json
import os
import sys
import re
import time


database_path = os.getcwd()
database_path = database_path + "\database"
sys.path.insert(0,database_path)

from pred_qry import * 
import requests as req

def data_post(file=None, username = None):
    

    start = time.time()

    # As a mock test, the data will be added to user 1 which is the admin account
    if username == None:
        userid = 1
    else:
        userid = req.post(dbserver_api, json=data_send).json()['data'][0][0]

    # Path to the file (for test) we will use a mock data file with the path below
    if file == None:
        path = os.path.abspath(os.getcwd())
        file_path = os.path.join(path,"data_collect\HUAWEI_HEALTH\Health detail data & description\health detail data.json")
        print(file_path)
        with open(file_path,"r") as file:
            data = json.load(file)
    else:
        dataStr = file.decode('utf-8')
        data = json.loads(dataStr)

    dbserver_api_mul = "http://127.0.0.1:5050/ins_mul"
    dbserver_api = "http://127.0.0.1:5050/qry"

    data_send = { 
        "query": "SELECT id from user WHERE username = {username}",
        "param_input" : { "username" : username }
        }
    
    data_list =[]
    number_of_data = 0
    num_error = 0
    batch_num = 0 # Each batch will have around 10000 data
    test_path = os.path.dirname(os.path.abspath(__file__)) + "/test.txt"
    with open(test_path,"w") as test_file:
        for element in data:
            TimeZone = element["timeZone"]

            break # To collect global data once

        for element in data:

            # different data type has different shape of data
            data_type = element["type"]

            # access to  "samplePoints"
            for point in element["samplePoints"]:


                # start_timestamp = element["startTime"]
                # end_timestamp = element["endTime"]

                if data_type == 16: # Blood oxygen saturation
                    value = point["value"]
                    value_dict = json.loads(value)
                    val = value_dict["avgSaturation"]
                    start_timestamp = point["startTime"]
                    end_timestamp = point["endTime"] 


                    #key = "Blood oxygen saturation"
                    #Avgsaturation =  re.sub("[^\d\.]", "", Avgsaturation) # Convert data into float
                    #print(Avgsaturation)

                    data_list += [(userid,data_type,start_timestamp,end_timestamp,val)]


                    # data_send = {
                    # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                    #     VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                    # "param_input" : {
                    #     "userid": userid, # To be determined
                    #     "health_id" : 16,
                    #     "starttime" : start_timestamp,
                    #     "endtime" : end_timestamp,
                    #     "datainfo" : '[{"description":'+key+',"value":'+str(Avgsaturation)+'}]',
                    #     }
                    # }
                    # res = req.post(dbserver_api, json=data_send)

                    # if res.status_code == 200: 
                    #     print({"status":"success"}) 
                    # else:
                    #     print({'status':'fail',
                    #         'fail code':f'{res.status_code}'})

                    # if res.status_code != 200:
                    #     return "Error : Data shape not good"

                if data_type == 200005: # Active hour


                    start_timestamp = point["startTime"]
                    end_timestamp = point["endTime"] 

                    # No need because always yes
                    # Is_active = point["value"]
                    # Is_active = re.sub("[^\d\.]", "", Is_active)
                    # res = No
                    # if Is_active == 1:
                    #     res = Yes 
                    
                    val = (end_timestamp - start_timestamp) #time elapsed

                    data_list += [(userid,data_type,start_timestamp,end_timestamp,val)]

                    # data_send = {
                    # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                    # VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                    # "param_input" : {
                    #     "userid": userid, # To be determined
                    #     "health_id" : 200005,
                    #     "starttime" : start_timestamp,
                    #     "endtime" : end_timestamp,
                    #     "datainfo" : '[{"description":'+point["key"]+',"time elapsed":'+str(time_elapsed)+'}]',
                    #     }
                    # }
                    # #print(time_elapsed)
                    # res = req.post(dbserver_api, json=data_send)

                    # if res.status_code == 200: 
                    #     print({"status":"success"}) 
                    # else:
                    #     print({'status':'fail',
                    #         'fail code':f'{res.status_code}'})

                    # if res.status_code != 200:
                    #     return "Error : Data shape not good"

                if data_type == 7:

                    start_timestamp = point["startTime"]
                    end_timestamp = point["endTime"] 

                    if (point["key"] == "DATA_POINT_DYNAMIC_HEARTRATE"):
                        data_type = 70
                    else:
                        data_type = 71 #"DATA_POINT_NEW_REST_HEARTRATE"

                    val = point["value"]

                    data_list += [(userid,data_type,start_timestamp,end_timestamp,val)]

                    # data_send = {
                    # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                    # VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                    # "param_input" : {
                    #     "userid": userid, # To be determined
                    #     "health_id" : 7,
                    #     "starttime" : start_timestamp,
                    #     "endtime" : end_timestamp,
                    #     "datainfo" : '[{"description":'+point["key"]+',"value":'+str(point["value"])+'}]',
                    #     }
                    # }

                    # res = req.post(dbserver_api, json=data_send)
                    # if res.status_code == 200: 
                    #     print({"status":"success"}) 
                    # else:
                    #     print({'status':'fail',
                    #         'fail code':f'{res.status_code}'})

                    # if res.status_code != 200:
                    #     return "Error : Data shape not good"
                
                if data_type == 9 : 

                    start_timestamp = point["startTime"]
                    end_timestamp = point["endTime"] 

                    val = (end_timestamp - start_timestamp) # time elapsed

                    if (point["key"] == "PROFESSIONAL_SLEEP_NOON"):
                        data_type = 90
                    elif (point["key"] == "PROFESSIONAL_SLEEP_SHALLOW"):
                        data_type = 91
                    elif (point["key"] == "PROFESSIONAL_SLEEP_DEEP"):
                        data_type = 92
                    elif (point["key"] == "PROFESSIONAL_SLEEP_DREAM"):
                        data_type = 93
                    else: #"PROFESSIONAL_SLEEP_WAKE"
                        data_type = 94

                    data_list += [(userid,data_type,start_timestamp,end_timestamp,val)]

                    # data_send = {
                    # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                    # VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                    # "param_input" : {
                    #     "userid": userid, # To be determined
                    #     "health_id" : 9,
                    #     "starttime" : start_timestamp,
                    #     "endtime" : end_timestamp,
                    #     "datainfo" : '[{"description":'+point["key"]+',"time elapsed":'+str(time_elapsed)+'}]',
                    #     }
                    # }
                    
                    # res = req.post(dbserver_api, json=data_send)
                    # if res.status_code == 200: 
                    #     print({"status":"success"}) 
                    # else:
                    #     print({'status':'fail',
                    #         'fail code':f'{res.status_code}'})

                    # if res.status_code != 200:
                    #     return "Error : Data shape not good"

                if data_type == 11:
                    
                    values = point['value']
                    values_dict = json.loads(values)
                    feature = values_dict['feature']

                    start_timestamp = point["startTime"]
                    end_timestamp = point["endTime"] 
                    
                    for i in range(len(feature)):

                        data_list += [(userid,11+i,start_timestamp,end_timestamp,feature[i])]

                        # data_send = {
                        # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                        # VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                        # "param_input" : {
                        #     "userid": userid, # To be determined
                        #     "health_id" : 11+i,
                        #     "starttime" : start_timestamp,
                        #     "endtime" : end_timestamp,
                        #     "datainfo" : '[{"description":stress data '+str(i)+',"value":'+str(feature[i])+'}]',
                        #     }
                        # }

                        # res = req.post(dbserver_api, json=data_send)

                        # if res.status_code == 200: 
                        #     print({"status":"success"}) 
                        # else:
                        #     print({'status':'fail',
                        #         'fail code':f'{res.status_code}'})

                        # if res.status_code != 200:
                        #     return "Error : Data shape not good"


                if data_type == 500005:

                    fields_metadata = point['fieldsMetadata']
                    fields_metadata_dict = json.loads(fields_metadata)


                    # Access the contents dynamically
                    if 'noonSleepInfo' in fields_metadata_dict:
                        noon_sleep_info = json.loads(fields_metadata_dict['noonSleepInfo'])
            
                        # Check if 'noonSleepTimeIntervalList' is present and extract it
                        if 'noonSleepTimeIntervalList' in noon_sleep_info:
                            val = noon_sleep_info['noonSleepTotalTime']
                            for values in noon_sleep_info["noonSleepTimeIntervalList"]:
                                start_timestamp = values['startTime']
                                end_timestamp = values['endTime']

                            data_list += [(userid,data_type,start_timestamp,end_timestamp,val)]


                            # data_send = {
                            # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                            # VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                            # "param_input" : {
                            #     "userid": userid, # To be determined
                            #     "health_id" : 500005,
                            #     "starttime" : start_timestamp,
                            #     "endtime" : end_timestamp,
                            #     "datainfo" : '[{"description":noonSleepTime,"value (min)":'+str(noon_sleep_total_time)+'}]',
                            #     }
                            # }
                            # res = req.post(dbserver_api, json=data_send)

                            # if res.status_code == 200: 
                            #     print({"status":"success"}) 
                            # else:
                            #     print({'status':'fail',
                            #         'fail code':f'{res.status_code}'})

                            # if res.status_code != 200:
                            #     return "Error : Data shape not good"

                if data_type == 300002:
                    value = point['value']
                    value_dict = json.loads(value)
                    duration_value = value_dict.get('durationUserValue',0)
                    active_user_value = value_dict.get('activeUserValue',0)
                    step_user_value = value_dict.get('stepUserValue',0)
                    calorie_spent = value_dict.get('calorieUserValue',0)

                    if duration_value != 0:
                        data_list += [(userid,data_type,start_timestamp,end_timestamp,duration_value)]
                    if active_user_value != 0:
                        data_list += [(userid,data_type,start_timestamp,end_timestamp,active_user_value)]
                    if step_user_value != 0:
                        data_list += [(userid,data_type,start_timestamp,end_timestamp,step_user_value)]
                    if calorie_spent != 0:
                        data_list += [(userid,data_type,start_timestamp,end_timestamp,calorie_spent)]


                    # data_send = {
                    # "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,datainfo)\
                    # VALUES({userid},{health_id},{starttime},{endtime},{datainfo})",
                    # "param_input" : {
                    #     "userid": userid, # To be determined
                    #     "health_id" : 300002,
                    #     "starttime" : start_timestamp,
                    #     "endtime" : end_timestamp,
                    #     "datainfo" : "[{\"active band time (hour)\":"+str(active_user_value)
                    #     +",\"number of steps\":"+str(step_user_value)+",\"calorie spent\":"+str(calorie_spent)+",\"Average duration (min)\":"+str(duration_value)+"}]",
                    #     }
                    # }

                    # res = req.post(dbserver_api, json=data_send) 
                    # if res.status_code == 200: 
                    #     print({"status":"success"}) 
                    # else:
                    #     print({'status':'fail',
                    #         'fail code':f'{res.status_code}'})

                    # if res.status_code != 200:
                    #     return "Error : Data shape not good"
                        
            if len(data_list) >= 10000: #send data every 10000
                data_send = {
                    "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,value)\
                    VALUES({userid},{health_id},{starttime},{endtime},{value})",
                    "data_list" : data_list
                }
                batch_num += 1
                res = req.post(dbserver_api_mul,json=data_send)

                if res.status_code != 200:
                    num_error += 1
                    test_file.write("Error : Data are not in a good shape in batch: "+str(batch_num)+", elapsed time from start: "+ str((time.time()-start))+" s\n")
                else:
                    test_file.write("Success: batch: "+str(batch_num)+" has been successfully added to the database in "+str((time.time()-start))+" s\n")

                
                
                number_of_data += len(data_list)
                data_list = []
                
        
        # in case if at the end we do not achieve 10000 element in data_list
        data_send = {
                    "query": "INSERT INTO DeviceData (userid, health_id,starttime,endtime,value)\
                    VALUES({userid},{health_id},{starttime},{endtime},{value})",
                    "data_list" : data_list
                }
        batch_num += 1
        res = req.post(dbserver_api_mul,json=data_send)

        if res.status_code != 200:
            num_error += 1
            test_file.write("Error : Data are not in a good shape in batch: "+str(batch_num)+", elapsed time from start: "+ str((time.time()-start))+" s\n")
        else:
            test_file.write("Success: batch: "+str(batch_num)+" has been successfully added to the database in "+str((time.time()-start))+" s\n")
        
        number_of_data += len(data_list)
        test_file.write(str(number_of_data)+" data has been added to the database in "+ str((time.time()-start))+" s, a total of "+str(num_error)+" errors\n")
    return "File has been uploaded"


if __name__=="__main__":
    data_post()
    