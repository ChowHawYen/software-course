import json
from datetime import datetime

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        return super(DateTimeEncoder, self).default(obj)
    
user_query = "SELECT * FROM users WHERE \
            userid = {userid}"

user_login_query = "SELECT * FROM Account WHERE \
            username = {username}"

get_all_account = "SELECT * FROM Account"

register_new_account = 'INSERT INTO Account (username,password,userid,is_premium)\
            VALUES({username},{password},{userid},{is_premium})'
