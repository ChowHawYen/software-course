from config_test import *
from flask import Flask
import time
import random
from user_management import is_user_registered

timestamp=0

def test_register():
    global timestamp
    """ 测试注册功能能否正常运行"""
    res=req.post(controller_url + '/register',json=register_data_send).json()
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
    assert (res['status']=='success' )  
    res=req.post(controller_url + '/register',json=register_data_send).json()
    assert (res['status']=='fail' and res["message"]=="用户已存在")

def test_login():
    """ 测试登录功能能否正常运行"""
    res = req.post(controller_url + '/login',json=login_data_send).json()
    assert (res['status']=='success' ) 
    assert (res['content']['name']=='Dominique2')                        #成功登录
    res = req.post(controller_url + '/login',json=fail_login_data_send).json()
    assert (res['status']=='fail' and res['message']=='密码错误')        #密码错误
    res = req.post(controller_url + '/login',json=fail2_login_data_send).json()
    assert (res['status']=='fail' and res['message']=='用户不存在')        #用户不存在

def test_search():
    """ 测试搜索功能能否正常运行"""
    local_compare=compare_search          
    res = req.get(controller_url + '/search',params=username_data_send).json()
    local_compare['created_date']=timestamp
    assert (res['status']=='success' )         
    assert(res['content']==local_compare)

def test_modify():
    """测试信息修改功能能否正常运行"""
    random_height=int(140+random.random()*60)
    local_compare=compare_modify
    local_compare['height']=random_height
    res = req.post(controller_url + '/modify',json=local_compare).json()
    assert (res['status']=='success')
    res=req.get(controller_url + '/search',params=username_data_send).json()
    assert (res['content']['height']==random_height)

def test_delete():
    """ 测试注销功能能否正常运行"""
    res = req.post(controller_url + '/delete',json=username_data_send).json()
    assert (res['status']=='success' ) 
    res=is_user_registered(username_data_send['username'])
    assert (res['content']==False)

if __name__ == '__main__':
    pass

