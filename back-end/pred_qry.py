
# initialize all query
table_query="SELECT name FROM sqlite_master WHERE type = 'table' AND name='user'"

user_query = "SELECT * FROM user WHERE \
            username = {username}"

placeholders = ["name", "birthday", "height", "weight", "h_info", "gender", "been_drinking", "been_smoking", "created_date","username", "password", "phone_number","email","is_premium"]

user_login_query = "SELECT * FROM user WHERE \
            username = {username}"

get_all_account = "SELECT * FROM user"

search_delete=["password","is_premium","h_info"]

bool_check=["been_drinking", "been_smoking"]

register_new_account =  "INSERT INTO user (name, birthday, height, weight, h_info, gender, been_drinking, been_smoking, username, password,phone_number,email, is_premium)\
VALUES ({name}, {birthday}, {height}, {weight}, {h_info}, {gender}, {been_drinking}, {been_smoking}, {username}, {password},{phone_number},{email}, {is_premium})"

delete_account="DELETE FROM user WHERE username = {username}"

modify_user_password = "UPDATE user SET password={password} WHERE username = {username}"

modify_userinfo = "UPDATE user SET name={name},\
                                   birthday={birthday}, \
                                   height={height},\
                                   weight={weight},\
                                   gender={gender},\
                                   been_drinking={been_drinking},\
                                   been_smoking={been_smoking},\
                                   phone_number={phone_number},\
                                   email={email}\
                                   WHERE username = {username}"

time_range_query = "SELECT MIN(starttime), MAX(starttime) FROM DeviceData WHERE userid = {userid}"