from flask import Flask, render_template,request,jsonify
from pred_qry import *  # import predefined query, optional
import requests as req # import requests lib
from emailsend import *
from user_management import *
from flask_caching import Cache
from flask_bcrypt import Bcrypt
from data_collection import data_post
#from getinput import getinput
#from . import health_model
from model import predict as predict_logic
import logging,time
from datetime import datetime,timezone

# Create a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)
# Create a file handler
file_handler = logging.FileHandler('controller_log.log')
logger.addHandler(file_handler)
file_format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(file_format)
logger.addHandler(file_handler)

app=Flask(__name__)
dbserver_api = "http://127.0.0.1:5050/qry"
dbcreate_api = "http://127.0.0.1:5050/recdb"
app.config["SECRET_KEY"] = "any random string"
app.config["CACHE_TYPE"] = "FileSystemCache" 
app.config['CACHE_DIR'] = 'cache' # path to your server cache folder
app.config['CACHE_DEFAULT_TIMEOUT'] = 120  # 缓存失效时间为2分钟
app.config['CACHE_THRESHOLD'] = 100000
cache=Cache(app)
bcrypt = Bcrypt(app)


#login function:获取前端json数据，把userid与数据库比对，若数据库不存在
@app.route('/login',methods=['POST'])
def login():
    try:
        return login_logic(bcrypt)
    except Exception as err:
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500

#注册新用户
@app.route('/register',methods=['POST'])
def register():
    try:
        return register_logic(bcrypt)
    except Exception as err:
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500


#查询信息
@app.route('/search',methods=['GET'])
def search():
    try:
        return search_logic()
    except Exception as err:
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500 

#修改信息
@app.route('/modify',methods=['POST'])
def modify():
    try:
        return modify_logic()
    except Exception as err:
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500 
    
    
#注销账号
@app.route('/delete',methods=['POST'])
def delete():
    try:
        return delete_logic()
    except Exception as err:
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500

#找回密码
@app.route('/retrieve',methods=['GET'])
def retrieve():
    try:
        is_db_exists()
    except Exception as err :
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({"status":"fail",
                        "message":"数据库访问失败"}),500
    username=request.args.get('username')
    data_send={"query":user_query,
            "param_input":{'username':username}
                }
    res=req.post(dbserver_api,json=data_send)
    if res.status_code == 200: 
        res=res.json()
        if res['data'] == []:
            return jsonify({"status":"fail",
                            "message":"用户不存在"})
        else:
            user_data={key:res['data'][0][i+1] for i,key in enumerate(placeholders)}
    code=random.randint(100000,999999)
    cache.set(username, code)
    try:
        send_email(code,user_data["email"])
        return jsonify({"status":"success",
                        "content":{"email":user_data["email"]}
                        })
    except Exception as err:
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({"status":"fail",
                        "message":"验证码发送失败"}),500

    
#找回密码，验证码验证
@app.route('/retrieve/varify',methods=['POST'])
def varify():
    try:
        is_db_exists()
    except Exception as err :
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({"status":"fail",
                        "message":"数据库访问失败"}),500
    retrieve_data=request.get_json()
    code=retrieve_data['code']
    username=retrieve_data['username']
    new_password=retrieve_data['new_password']
    if str(cache.get(username))==code:
        send_data=bcrypt.generate_password_hash(new_password).decode('utf-8')
        data_send={"query":modify_user_password,
            "param_input":{'username':username,
                'password':send_data}
                }
        res=req.post(dbserver_api,json=data_send)
        if res.status_code == 200: 
            return jsonify({"status":"success",
                            "message":"密码已修改"})
        else:
            return jsonify({"status":"fail",
                        "message":"数据库访问失败"}),500
    else :
        return jsonify({"status":"fail",
                        "message":"验证码不正确"})


@app.route('/upload',methods=['POST'])
def data_collection():
    try:
        is_db_exists()
    except Exception as err :
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({"status":"fail",
                        "message":"数据库访问失败"}),500
    username=request.form['username']
    collection_data=request.files['file']
    if collection_data:
        file=collection_data.read()
    try:
        data_post(file,username)
        return jsonify({"status":"success",
                        "message":"数据存入成功"})
    except Exception as err :
        logger.error("An error occurred: %s", err, exc_info=True)
        return jsonify({"status":"fail",
                        "message":"数据存入失败"}),500


@app.route('/predict',methods=['POST','GET'])
def predict():
    if request.method == 'GET':              #get方法，获取该username的最早时间与最晚时间
        try:
            is_db_exists()
        except Exception as err :
            logger.error("An error occurred: %s", err, exc_info=True)
            return jsonify({"status":"fail",
                            "message":"数据库访问失败"}),500
        username=request.args.get('username')
        data_send = { 
            "query": "SELECT id from user WHERE username = {username}",
            "param_input" : { "username" : username }
            }
        
        userid = req.post(dbserver_api, json=data_send).json()['data'][0][0]
        data_send={"query":time_range_query,
                "param_input":{'userid':userid}
                    } 
        res=req.post(dbserver_api,json=data_send).json()
        [minDatestamp, maxDatestamp] = res['data'][0]

        if minDatestamp == None or maxDatestamp == None:
            return jsonify({"status":"no_data",
                            "message":"没有数据，请上传"})

        min_datetime_object = datetime.fromtimestamp(minDatestamp / 1000)
        minDate=min_datetime_object.strftime('%Y-%m-%d')

        max_datetime_object = datetime.fromtimestamp(maxDatestamp / 1000)
        maxDate=max_datetime_object.strftime('%Y-%m-%d')

        return jsonify({"status":"success",
                        "content":{"minDate":minDate,
                                   "maxDate":maxDate}})



    if request.method == 'POST':             #POST方法，接受起始与结束时间，返回报告
        try:
            is_db_exists()
        except Exception as err :
            logger.error("An error occurred: %s", err, exc_info=True)
            return jsonify({"status":"fail",
                            "message":"数据库访问失败"}),500
        user=request.get_json()
        username=user['username']
        user_registered=is_user_registered(username)
        if user_registered['status']=='success':
            if not user_registered['content']:
                return jsonify({"status":"fail",
                                "message":"用户不存在"})
        else:
            return jsonify({"status":"fail",
                            "message":user_registered['message']})
        try:
            start_time=user['startTime']
            start_time_tuple = time.strptime(start_time, "%Y-%m-%d")
            end_time=user['endTime']
            end_time_tuple=time.strptime(end_time, "%Y-%m-%d")
            start_timestamp = int(time.mktime(start_time_tuple))*1000-28800
            end_timestamp = int(time.mktime(end_time_tuple))*1000-28800
        except Exception as err :
            logger.error("An error occurred: %s", err, exc_info=True)
            return jsonify({"status":"fail",
                            "message":"时间戳格式出错"})

        data_send = { 
            "query": "SELECT id from user WHERE username = {username}",
            "param_input" : { "username" : username }
            }
        
        userid = req.post(dbserver_api, json=data_send).json()['data'][0][0]

        try:
            return jsonify({"status":"success",
                            "content":predict_logic(userid=userid,starttime=start_timestamp,endtime=end_timestamp)})
        except Exception as err :
            logger.error("An error occurred: %s", err, exc_info=True)
            return jsonify({"status":"fail",
                            "message":"数据读取失败"}),500




if __name__ == '__main__':
    app.run(host='127.0.0.1', threaded = False)
