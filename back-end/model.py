import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import json
import os
import requests as req

score_goal=['sleep','activity','stress']
advice={
    'sleep':{"excellent_sleep":"好消息！你的睡眠质量非常好。继续保持规律的睡眠时间表，并确保你的睡眠环境舒适。这将有助于维持良好的睡眠质量。",
             "good_sleep":"你的睡眠质量还不错，但可以留意一些细节。尽量避免在睡前摄入咖啡因或大量食物，并保持放松的睡前活动。",
             "average_sleep":"你的睡眠质量有些波动。考虑建立一个固定的睡眠时间表，避免使用电子设备，以帮助你更好地入睡。",
             "unsatisfactory_sleep":"你的睡眠质量可能需要一些改善。确保晚上放松并创造一个有利于睡眠的环境，同时规律锻炼有助于提高睡眠质量。"
    },
    "activity":{"excellent_activity":"恭喜！你的运动情况很出色。继续保持良好的运动习惯，这对身体和心理健康都有益处。建议保持多样化的运动，包括有氧运动、力量训练和伸展运动，以保持全面的健康。",
                "good_activity":"你的运动情况不错，但还有改进的空间。考虑增加运动时长或强度，以更全面地促进身体健康。",
                "average_activity":"你的运动情况有些不稳定。尽量保持每周规律的运动时间，这对保持健康非常重要。建议寻找喜欢的运动方式，使运动变得更有趣和可持续。",
                "unsatisfactory_activity":"运动对身体和心理健康至关重要。考虑增加每周的运动时长，逐渐增加活动量，并选择适合你的运动方式。"
    },
    "stress":{
        "excellent_stress":"检测显示你的压力水平较低，这是很好的。继续保持良好的应对压力的方法，比如定期休息和放松技巧。建议保持这种良好的状态，因为低压力有助于身心健康。",
        "good_stress":"你的压力水平有些波动。尽量识别并采用适合你的压力管理方法，如运动、冥想或放松技巧，以降低压力水平。",
        "average_stress":"如果你的评分较高，可能意味着你正在面临较大压力。建议你寻求有效的压力管理方法，如规律的锻炼、深呼吸或与他人分享感受。",
        "unsatisfactory_stress":"高压力水平对健康有害，因此需要寻求帮助并采取积极的措施来减轻压力。可以考虑与专业人士交流，寻求支持和建议。"
    }
}




input_size = 30  # 每个时间段的特征数量或序列长度
output_size = 3  # 输出特征的固定长度
hidden_size = 128  # LSTM隐藏层大小

mean_target=np.array([77.38461538, 70.17182131, 49.26116838])
std_target=np.array([4.91152899, 3.37300314,  6.10366692])

class HeartRateModel(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(HeartRateModel, self).__init__()
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(input_size, hidden_size, batch_first=True)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, x):
        h0 = torch.zeros(1, x.size(0), self.hidden_size).to(x.device)
        c0 = torch.zeros(1, x.size(0), self.hidden_size).to(x.device)
        x=x.to(torch.float32)
        out, _ = self.lstm(x, (h0, c0))
        out = out[:, -1, :]
        out = self.fc(out)
        return out
    



def predict(userid=1,starttime=1664409600000,endtime=1664496000000,model_path='model'):
    model = HeartRateModel(input_size, hidden_size, output_size)
    model.load_state_dict(torch.load(model_path))
    model.eval()

    const_daytime=24*3600*1000
    All_out=list()
    for Day_start_datetime in range(starttime,endtime+1,const_daytime):
        day_starttime=Day_start_datetime
        day_endtime=day_starttime+const_daytime
        out=dict()
        out={'sleep':{},'activity':{},'stress':{}}
        query = "Select value from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "healthid": 70,     #heartrate
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                } 
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()
        rows = len(seq_data['data'])  // 30
        inputmatrix = []
        for i in range(rows):
            row = seq_data['data'][i * 30: (i + 1) * 30]
            inputmatrix.append(row)
        inputmatrix=np.squeeze(np.array(inputmatrix))
        predicted_output = np.round(model(torch.tensor([inputmatrix])).detach().numpy()*std_target+mean_target)
        predicted_output[predicted_output>100]=100
        predicted_output[predicted_output<0]=0
        predicted_output=predicted_output.tolist()
        
        out['sleep']['score'] = predicted_output[0][0]
        if predicted_output[0][0]>=90:
            out['sleep']['advice']=advice["sleep"]["excellent_sleep"]
        elif predicted_output[0][0]>=75:
             out['sleep']['advice']=advice["sleep"]["good_sleep"]
        elif predicted_output[0][0]>=60:
             out['sleep']['advice']=advice["sleep"]["average_sleep"]
        else :
             out['sleep']['advice']=advice["sleep"]["unsatisfactory_sleep"]

        out['activity']['score'] = predicted_output[0][1]
        if predicted_output[0][1]>=80:
            out['activity']['advice']=advice["activity"]["excellent_activity"]
        elif predicted_output[0][1]>=60:
             out['activity']['advice']=advice["activity"]["good_activity"]
        elif predicted_output[0][1]>=40:
             out['activity']['advice']=advice["activity"]["average_activity"]
        else :
             out['activity']['advice']=advice["activity"]["unsatisfactory_activity"]

        out['stress']['score'] = predicted_output[0][2]
        if predicted_output[0][2]<=30:
            out['stress']['advice']=advice['stress']["excellent_stress"]
        elif predicted_output[0][2]<=40:
             out['stress']['advice']=advice['stress']["good_stress"]
        elif predicted_output[0][2]<=50:
             out['stress']['advice']=advice['stress']["average_stress"]
        else :
             out['stress']['advice']=advice['stress']["unsatisfactory_stress"]


        query = "Select value from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "healthid": 90,     #noonsleep
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                } 
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()
        out['noontime']=np.sum(np.array(seq_data['data']))
        


        query = "Select value from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "healthid": 91,     #shallowsleep
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                } 
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()
        out['shallow_sleep_time']=np.sum(np.array(seq_data['data']))

        query = "Select value from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "healthid": 92,     #deepsleep
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                } 
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()
        out['deep_sleep_time']=np.sum(np.array(seq_data['data']))

        query = "Select value from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "healthid": 93,     #dreamsleep
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                } 
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()
        out['dream_sleep_time']=np.sum(np.array(seq_data['data']))

        #展示用睡眠数据
        query = "Select health_id,starttime,endtime from DeviceData Where userid = {userid} and health_id >={starthealthid} and health_id <={endhealthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "starthealthid": 90,     
                    "endhealthid":93,
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                }
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()['data']
        transposed_seq_data = np.array(seq_data).T.tolist()
        out['time_graph']=transposed_seq_data[0:2]

        query = "Select value,starttime,endtime from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
        data_send = {
                "query": query,
                "param_input":{
                    "userid": userid,
                    "healthid": 70,     #heartrate
                    "starttime": day_starttime,
                    "endtime":  day_endtime
                }
                } 
        dbserver_api = "http://127.0.0.1:5050/qry"
        res = req.post(dbserver_api, json=data_send)
        seq_data=res.json()['data']
        transposed_seq_data = np.array(seq_data).T.tolist()
        out['heart_rate']=transposed_seq_data[0:2]

        All_out.append(out)



    return All_out