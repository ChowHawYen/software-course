from flask import Flask, render_template, request, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import FileField, SubmitField
from werkzeug.utils import secure_filename
import data_collection
import os
import time


app = Flask(__name__)
app.config['SECRET_KEY'] = 'supersecretkey'
# Définir le chemin d'accès au dossier de téléchargement
UPLOAD_FOLDER = 'uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

class UploadFileForm(FlaskForm):
    file = FileField("File")
    submit = SubmitField("Upload File")


@app.route('/',methods=["GET","POST"])

@app.route('/home',methods=["GET","POST"])
def home():
    form = UploadFileForm()
    if form.validate_on_submit():
        file = form.file.data
        name = secure_filename(os.path.splitext(os.path.basename(file.filename))[0] + "1" + os.path.splitext(os.path.basename(file.filename))[1])
        file.save(os.path.join(os.path.abspath(os.path.dirname(__file__)),app.config['UPLOAD_FOLDER'],secure_filename(name)))
        path = os.path.abspath("data_collect/uploads/"+name)
        
        res = data_collection.data_post(path)
        return res
    return render_template('upload.html', form=form)


if __name__ == '__main__':
    app.run(debug=True)
