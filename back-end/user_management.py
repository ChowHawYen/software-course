from flask import request,jsonify
from pred_qry import *  # import predefined query, optional
import requests as req # import requests lib
from emailsend import *
import random

dbserver_api = "http://127.0.0.1:5050/qry"
dbcreate_api = "http://127.0.0.1:5050/recdb"

#判断数据库是否存在
def is_db_exists():
    data_send={"query":table_query,
               "param_input": None
    }
    res=req.post(dbserver_api,json=data_send).json()
    if res["data"] ==[]:
        req.post(dbcreate_api)

def is_user_registered(username):
    data_send = {
            "query": user_query,
            "param_input": {"username": username}
            }
    res = req.post(dbserver_api, json=data_send)
    if res.status_code == 200: 
        received_data = res.json()
    else:
        return {'status':'fail',
            'message':'数据库访问出错'},500
    if received_data['data']==[]:                                    #如果没取到数据，则不存在该username
        return {'status':'success',
            'content':False}
    return {'status':'success',
            'content':True}

#登录
def login_logic(bcrypt):
    #从前端获取用户id与psw
    is_db_exists()
    user=request.get_json()
    username=user['username']
    psw=user['password']
    #从数据库取出用户id和psw
    data_send = {
            "query": user_query,
            "param_input": {"username": username}
            }
    #data_send=json.dumps(data_send)
    
    res = req.post(dbserver_api, json=data_send)
    if res.status_code == 200: 
        received_data = res.json()
    else:
        return {'status':'fail',
                'message':'数据库访问出错'},500
    if received_data['data']==[]:                                    #如果没取到数据，则不存在该username
        return_data={'status':'fail',
                    'message':"用户不存在"}
    else:
        try:
            received_data={key:received_data['data'][0][i+1] for i,key in enumerate(placeholders)}
            if bcrypt.check_password_hash(str(received_data["password"]),str(psw)):         #received_data["password"，密码存储在该项中
                return_data={"status":"success",
                             "content":{"name":received_data["name"]}}
            else:
                return_data={'status':'fail',
                            'message':"密码错误"}
        except Exception as e:
            return jsonify({'status':'fail',
                        'message':"密码认证出错"}),500
    try:
        return jsonify(return_data)
    except Exception as e:
        return jsonify({'status':'fail',
                    'message':"登录状态返回出错"}),500

#注册
def register_logic(bcrypt):
    is_db_exists()
    register_data=request.get_json()
    username=register_data['username']
    #先查询数据库，该用户是否存在，若存在，返回fail
    user=is_user_registered(username)
    if user['status']=='success':
        if user['content']:
            return jsonify({"status":"fail",
                            "message":"用户已存在"})
    else:
        return jsonify({"status":"fail",
                        "message":user['message']}),500
    #该用户不存在，创建新用户
    register_data["is_premium"]=False
    send_data={key:None for key in placeholders}                #placeholders从该变量获取应注册的属性
    for key in register_data:
        send_data[key]=register_data[key]
    send_data["password"]=bcrypt.generate_password_hash(register_data["password"]).decode('utf-8')     #加密
    for key in bool_check:                                          #检查"been_drinking", "been_smoking"是否为bool量或者none
        if key in send_data:
            if send_data[key]==None:
                send_data[key]=False
            elif not isinstance(send_data[key],bool):
                return jsonify({'status':'fail',
                             'message':'传输数据类型出错'}),500
    try:
        send_data["gender"]=int(send_data["gender"])                 #检查gender是否为数字
    except ValueError:
        return jsonify({'status':'fail',
                             'message':'传输数据类型出错'}),500
    data_send = {
            "query": register_new_account,
            "param_input": send_data
            }
    res = req.post(dbserver_api, json=data_send)
    if res.status_code == 200: 
        return jsonify({"status":"success"})    
    else:
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500

#查找    
def search_logic():
    is_db_exists()
    username=request.args.get('username')
    data_send={"query":user_query,
               "param_input":{'username':username}
    }
    res=req.post(dbserver_api,json=data_send)
    if res.status_code == 200: 
        res=res.json()
        if res['data'] == []:
            return jsonify({"status":"fail",
                            "message":"用户不存在"})
        else:
            received_data={key:res['data'][0][i+1] for i,key in enumerate(placeholders)}

            for key in search_delete :
                if key in received_data:
                    del received_data[key] 
            try:
                received_data['gender']=int(received_data['gender'])
                return jsonify({"status":"success",
                         "content":received_data})
            except ValueError:
                pass           
    return jsonify({'status':'fail',
               'message':'数据库访问失败'}),500    

#修改信息    
def modify_logic():
    is_db_exists()
    modify_data=request.get_json()
    username=modify_data['username']
    user=is_user_registered(username)
    if user['status']=='success':
        if not user['content']:
            return jsonify({"status":"fail",
                            "message":"用户不存在"})
    else:
        return jsonify({"status":"fail",
                        "message":user['message']})
    for key in bool_check:
        if key in modify_data:
            if not isinstance(modify_data[key],bool):
                return jsonify({'status':'fail',
                                'message':'传输数据类型出错'}),500
    data_send={
        "query":modify_userinfo,
        "param_input":modify_data
    }
    res=req.post(dbserver_api,json=data_send)
    if res.status_code == 200: 
        return jsonify({"status":"success"})    
    else:
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500

#注销用户    
def delete_logic():
    is_db_exists()
    delete_data=request.get_json()
    username=delete_data['username']
    user=is_user_registered(username)
    if user['status']=='success':
        if not user['content']:
            return jsonify({"status":"fail",
                            "message":"用户不存在"})
    else:
        return jsonify({"status":"fail",
                        "message":user['message']})
    data_send={"query":delete_account,
               "param_input":{'username':username}
    }
    res=req.post(dbserver_api,json=data_send)
    if res.status_code == 200: 
        return jsonify({"status":"success"})    
    else:
        return jsonify({'status':'fail',
                'message':'数据库访问失败'}),500   
