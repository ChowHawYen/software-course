import requests as req
import pytest
import json

controller_url="http://127.0.0.1:5000/"

compare_search={    
    "been_drinking": 1,
    "been_smoking": 0,
    "birthday": "2023-11-12",
    "created_date": "2024-01-08 14:10:31",
    "email": "13637910608@163.com",
    "gender": 4,
    "height": 178.0,
    "name": "Dominique2",
    "phone_number": None,
    "username": "Huang",
    "weight": 68.0}

compare_modify= {
                "name": "Dominique2",
                "birthday": "2023-11-15",
                "height": 178,
                "weight": 68,
                "h_info": "Healthy",
                "gender": 99,
                "been_drinking": False,
                "been_smoking":False,
                "username": "Huang",
                "phone_number":"123",
                "email":"13637910608@163.com",
                "is_premium": True
                }

register_data_send= {
                "name": "Dominique2",
                "birthday": "2023-11-12",
                "height": 178,
                "weight": 68,
                "gender": "4",
                "been_drinking": True,
                "username": "Huang",
                "password": "password",
                "email":"13637910608@163.com"
                }

login_data_send={
    "username":"Huang",
    "password": "password"
}

fail_login_data_send={
    "username":"Huang",
    "password": "123"
}

fail2_login_data_send={
    "username":"Huang123",
    "password": "123"
}

username_data_send={'username': "Huang"}