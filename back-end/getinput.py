import requests as req
from datetime import datetime
import re
import os
import json

def getinput(userid=1,starttime=1664413920,endtime=1764413920,input_path='input.json'):
    dbserver_api = "http://127.0.0.1:5050/"
    query = "SELECT height, weight, gender, birthday, been_drinking FROM user WHERE id = {userid}"
    data_send = {
                "query": query,
                "param_input":{
                    "userid":userid
                }
                } 
    dbserver_api = "http://127.0.0.1:5050/qry"
    res = req.post(dbserver_api, json=data_send)
    feature_data=res.json()['data'][0]
    if feature_data[3]:
        feature_data[3]=2023-datetime.strptime(feature_data[3],'%Y-%m-%d').year
    feature_data=[0 if x==''else x for x in feature_data]
    dict_data={'height':feature_data[0],'weight':feature_data[1],'age':feature_data[3],'resting_blood_pressure':feature_data[4],'gender':feature_data[2]}
    query = "Select datainfo from DeviceData Where userid = {userid} and health_id = {healthid} and starttime >= {starttime} and endtime <= {endtime}"
    data_send = {
            "query": query,
            "param_input":{
                "userid": userid,
                "healthid": 7,
                "starttime": starttime,
                "endtime":  endtime
            }
            } 
    dbserver_api = "http://127.0.0.1:5050/qry"
    res = req.post(dbserver_api, json=data_send)
    seq_data=res.json()
    list=[]
    for text in seq_data['data']:
        decimal_number = re.search(r'\d+\.\d+', text[0])
        if decimal_number:
            value=float(decimal_number.group())
        list.append(value)
        if len(list) >= 2000:
            break;
    dict_data['heart_rate']=list

    l=[dict_data]
    if os.path.exists(input_path):
        open(input_path,'w').close()
    with open(input_path,'w') as input_file:
        json.dump(l,input_file)



